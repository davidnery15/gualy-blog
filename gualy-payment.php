<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/footerLink.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="gualyPayment">
    <div class="gualyPaymentCont">
      <h2 class="titleSection">GUALY PAYMENT</h2>
      <p><strong>Gualy Payment</strong> fue creada el 22 de agosto del año 2017 y es liderada por un equipo de expertos en comercio, marketing, finanzas y tecnología, que, teniendo como misión generar innovadoras soluciones financieras, han logrado engranar sus conocimientos y experiencias para hacer de esta empresa una realidad tangible y una gran promesa de desarrollo de productos que van a revolucionar la manera en que manejamos nuestras finanzas a través de medios digitales.</p>
      <p>Esta empresa FINTECH, se especializa en innovación tecnológica y financiera, transformando ideas en modelos de negocio sustentables, a través del desarrollo de productos, conectándolos con el sector financiero para acelerar su entrada y escalamiento en el mercado.&nbsp; La visión de la organización es liderar soluciones financieras innovadoras en Venezuela y Latinoamérica al generar productos de valor para las comunidades, clientes y socios; desarrollando ideas sustentables en un ambiente conectado con el entorno emprendedor, tecnológico, académico y financiero.</p>
      <p>Nuestro éxito depende de relaciones humanas, directas y colaborativas con los diferentes actores del mercado; nos conectamos personalmente con los inversores de nuestro producto a través de líneas de atención para ofrecerles constante feedback sobre el status del negocio.</p>
      <p>Con base en nuestra experiencia para el desarrollo de productos tecnológicos, apalancados en el talento humano, conocimiento, competencias tecnológicas y relaciones con el entorno emprendedor, hemos creado nuestra primera solución tecnológica y financiera en Venezuela con alto potencial de escalabilidad en el mercado latinoamericano, la aplicación móvil y web Gualy.</p>
      <p>Contamos con un equipo de profesionales apasionados del emprendimiento, capaces de responder a constantes retos de negocios tecnológicos; de esta manera conforma un entramado organizacional capaz de desarrollar las relaciones colaborativas con grupos de interés, solucionar problemas de manera creativa, tomar decisiones en un entorno de verdadero diálogo y trabajo colectivo, implementar puntos de control interno, acceder a nuevas redes de contactos y por último, promover sentimientos de solidaridad y apoyo los distintos grupos de personas que intervienen en todos nuestros procesos.</p>
      <p>Con base en las premisas mencionadas, nuestro equipo ha desarrollado competencias personales, sociales, intelectuales y de gestión en: negocios digitales, talento humano, mercadeo y branding, finanzas, promoción del emprendimiento e innovación, diseño de estrategias de negocios. Promovemos conductas, valores y principios que fortalecen nuestra cultura corporativa; por ello, las actividades de desarrollo, promociones, comerciales y demás servicios generales se caracterizan por la transparencia y equidad en las relaciones de la directiva y socios con el talento humano, clientes de negocios, usuarios, aliados comerciales y demás grupos de interés.</p>
      <p>Maracaibo fue la primera ciudad de Venezuela en tener electricidad, tranvía, banco, telégrafo, teléfono y proyección de películas, por lo que no es una sorpresa que ahora sea la cuna y origen de esta empresa que seguirá trabajando por desarrollar soluciones FINTECH de vanguardia que generen valor a todos los sectores de las comunidades.</p>
      <p>Trabajamos cada día para mantener viva la llama de la innovación tecnológica en Venezuela, llevando nuestros modelos de negocio y productos a cada rincón del país y el mundo.</p>
    </div>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/navBarShow.js"></script>
</body>

</html>