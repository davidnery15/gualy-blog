<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/footerLink.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="blog">
    <div class="blogCont">
      <h2>¿Podemos convertir la crisis del efectivo en una oportunidad para estar a la vanguardia tecnológica mundial? <img class="emoji" alt="🤔" src="https://s.w.org/images/core/emoji/11/svg/1f914.svg"></h2>
      <p class="cita">octubre 24, 2018 / 0 Comentarios / en Economía, Ecosistema financiero, Fintech / por Ruth Rodríguez</p>
      <blockquote>«El secreto del cambio es enfocar toda tu energía, no en la lucha contra lo viejo, sino en la construcción de lo nuevo». Sócrates</blockquote>
      <p class="blogContText">Actualmente muchos países están estudiando propuestas para sustituir el dinero en efectivo con el objetivo de desarrollar una política monetaria más eficiente que limite la evasión fiscal, elimine la falsificación y mejore el seguimiento de los movimientos financieros. En algunos de ellos la iniciativa ha sido tomada por el gobierno y en otros por actores financieros o comerciales.</p>
      <p class="blogContText">Estas propuestas proyectan grandes beneficios, pero también vislumbran dificultades tecnológicas y barreras culturales levantadas sobre una larga tradición de pagar con dinero en efectivo gozando de sus principales atractivos como lo son: liquidez y privacidad.</p>
      <img class="blogContImg" src="img/fintech-01.jpg">
      <h3>LOS PRIMEROS PASOS</h3>
      <p class="blogContText">Ya se observan cambios en el sistema financiero que van en esta dirección, especialmente en el proceso de pagos, que también incluye la nueva tendencia global para la producción de bienes y servicios donde el cliente está más informado, es más exigente, tiene un gran poder de influencia, es el líder y centro del proceso; rompiendo así el esquema tradicional de orientación al producto donde el cliente era considerado solo un elemento más.</p>
      <p class="blogContText">En consecuencia, se observan fenómenos como la desintermediación de actores financieros tradicionales y la reintermediación con nuevos competidores que entran al mercado para satisfacer la demanda de nuevos servicios con reglas distintas y ajustadas a las necesidades y deseos de los clientes. Así nacen los modelos de negocio disruptivos Fintech.</p>
      <p class="blogContText">Para adaptarse a esta nueva realidad y continuar conquistando a sus clientes, todo el sistema financiero debe hacer cambios rápidamente de forma y fondo, tomando conciencia de que ahora necesitan adecuar sus estructuras a nuevos métodos de pago como los móviles, ya sea con plataformas propias o forjando alianzas con otras billeteras digitales, con las cuales, además de satisfacer las necesidades de sus clientes, tocan a su puerta para conocerlos mejor y desarrollar estrategias comerciales más efectivas.</p>
      <h3>EN VENEZUELA</h3>
      <p class="blogContText">La poca disponibilidad del efectivo nos ha forzado a utilizar otros métodos de pago como transferencias electrónicas, puntos de venta (POS) y aplicaciones móviles bancarias con una frecuencia mucho mayor a la habitual, y la experiencia ha demostrado que han sido insuficientes para manejar la demanda de pagos.</p>
      <p class="blogContText">Un alivio a este problema podría encontrarse en las aplicaciones de pagos en versión móvil y web como Gualy, que permiten a las personas obtener su propia billetera digital en la que pueden colocar el dinero que necesitan para hacer pagos rápidamente con solo colocar un correo electrónico o escanear un código QR, emulando el proceso de guardar billetes y monedas en su billetera de bolsillo para hacer los pagos cotidianos.</p>
      <p class="blogContTextEnd">¡Regístrate en Gualy y prueba tu billetera digital gratis! 😉</p>
    </div>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/navBarShow.js"></script>
</body>

</html>