<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/styles.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="DownloadSignIn">
    <div class="DownloadSignInCont">
      <img src="img/Grupo 69.png" alt="gualy-phone">
      <div class="DownloadSignInMedia">
        <h2 class="mainTitle">Descarga y<br> regístrate</h2>
        <p class="subjectText">Toma de nuevo el control de tu tiempo y dinero.</p>
        <div class="DownloadSignInMediaImg">
          <a href="https://play.google.com/store/apps/details?id=com.humbee.gualy&hl=es" target="_blank">
            <img src="img/Grupo 82.svg" alt="google-play">
          </a>
          <a href="https://apps.apple.com/ve/app/gualy/id1463434706" target="_blank">
            <img src="img/Grupo 83.svg" alt="app-store">
          </a>
          <a href="https://app.gualy.com/" target="_blank">
            <img src="img/Grupo 183.svg" alt="app-gualy">
          </a>
        </div>
      </div>
    </div>
  </div>

  <div class="sendPay">
    <div class="sendPayCont">
      <div class="sendPayText">
        <h2 class="mainTitle">Envía pagos</h2>
        <p class="subjectText">Instantáneos a personas y comercios.</p>
      </div>
      <img src="img/Grupo 70.png" alt="gualy-phone">
    </div>
    <span class="sendPayContGradient">
  </div>

  <div class="receivePayCommerce">
    <div class="receivePay">
      <img src="img/Grupo 71.png" alt="gualy-phone">
      <div class="receivePayText">
        <h2 class="mainTitle">Recibe pagos</h2>
        <p class="subjectText">En segundos, sin límites y a cualquier hora.</p>
      </div>
    </div>
    <div class="commerce">
      <p class="titleSection">Comercios afiliados</p>
      <div class="commerceCont">
        <img src="img/estacionamiento lago mall.jpg" alt="commerce-logo">
        <img src="img/sambil.jpg" alt="commerce-logo">
        <img src="img/Galerias.jpg" alt="commerce-logo">
        <img src="img/yogourt-boom-gualy.jpg" alt="commerce-logo">
        <img src="img/logo_allgrill.jpg" alt="commerce-logo">
        <img src="img/Franco (1).jpg" alt="commerce-logo">
        <img src="img/profiles_f.jpg" alt="commerce-logo">
        <img src="img/asodepa.jpg" alt="commerce-logo">
        <img src="img/Mi ternerita Norte.jpg" alt="commerce-logo">
        <img src="img/Mi ternerita Loft.jpg" alt="commerce-logo">
        <img src="img/Viveres La Nueva Imagen-01-01.jpg" alt="commerce-logo">
        <img src="img/super-duper-gualy.jpg" alt="commerce-logo">
        <img src="img/fesca.jpg" alt="commerce-logo">
        <img src="img/rostrosradiantes.jpg" alt="commerce-logo">
        <img src="img/tutti-eventi-gualy.jpg" alt="commerce-logo">
        <img src="img/DELMAIZ.jpg" alt="commerce-logo">
        <img src="img/panaderia mamajulia-01.jpg" alt="commerce-logo">
        <img src="img/Vale parking VIP.jpg" alt="commerce-logo">
        <img src="img/jabibi.jpg" alt="commerce-logo">
        <img src="img/azuba.jpg" alt="commerce-logo">
        <img src="img/Formacion panaderia.jpg" alt="commerce-logo">
        <img src="img/Centor de ortodoncia.jpg" alt="commerce-logo">
        <img src="img/lapaleteria-gualy.jpg" alt="commerce-logo">
        <img src="img/comercio__0002_descarga.jpg" alt="commerce-logo">
        <img src="img/arquidiocesis.jpg" alt="commerce-logo">
        <img src="img/padre claret.jpg" alt="commerce-logo">
        <img src="img/lago-box-gualy.jpg" alt="commerce-logo">
        <img src="img/Lobby Café Hotel Tibisay.jpg" alt="commerce-logo">
        <img src="img/mi vaquita.jpg" alt="commerce-logo">
        <img src="img/Puerta 5 xpress.jpg" alt="commerce-logo">
        <img src="img/Automercado_la_economia.jpg" alt="commerce-logo">
        <img src="img/Panaderia el nuevo tiunfo-01.jpg" alt="commerce-logo">
        <img src="img/Z Car's-01.jpg" alt="commerce-logo">
        <img src="img/metalarte.jpg" alt="commerce-logo">
      </div>
    </div>
    <span class="receivePayCommerceGradient"></span>
  </div>

  <div class="news">
    <p class="titleSection">Noticias</p>
    <div class="newsCont">
      <iframe src="https://www.youtube.com/embed/cO0oK5xbwCE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      <div class="newsYtbLink">
        <a class="newsYtbMedia" href="http://www.hormigatv.com/gualy-la-billetera-virtual-que-se-adapta-a-venezuela/" target="_blank">
          <img src="img/photo4987915662135502861.jpg">
          <div class="newsYtbMediaText">
            <p>GUALY LA BILLETERA VIRTUAL QUE SE ADAPTA A VENEZUELA</p>
            <p>La billetera virtual, creada en Maracaibo, quiere solucionar las...</p>
          </div>
        </a>
        <a class="newsYtbMedia" href="https://www.panorama.com.ve/espectaculos/Emprendedores-venezolanos-lanzan-App-de-pagos-en-comercios-20181127-0059.html" target="_blank">
          <img src="img/photo4987915662135502862.jpg">
          <div class="newsYtbMediaText">
            <p>EMPRENDEDORES VENEZOLANOS LANZAN APP DE PAGOS EN COMERCIOS</p>
            <p>Emprendedores venezolanos lanzan al mercado una billetera digital...</p>
          </div>
        </a>
        <a class="newsYtbMedia" href="http://versionfinal.com.ve/tecnologia/gualy-la-billetera-digital-creada-por-emprendedores-venezolanos/" target="_blank">
        <img src="img/photo4987915662135502864.jpg">
          <div class="newsYtbMediaText">
            <p>GUALY, LA BILLETERA DIGITAL CREADA POR EMPRENDEDORES VENEZOLANOS</p>
            <p>Los usuarios crean sus cuentas desde un teléfono móvil o la web...</p>
          </div>
        </a>
        <a class="newsYtbMedia" href="https://www.estamosenlinea.com.ve/2018/11/16/gualy-nueva-app-de-pagos-en-comercios-en-venezuela" target="_blank">
        <img src="img/photo4987915662135502863.jpg">
          <div class="newsYtbMediaText">
            <p>GUALY NUEVA APP DE PAGOS EN COMERCIOS EN VENEZUELA</p>
            <p>Contenido deLos usuarios crean sus cuentas Gualy desde un teléfono...</p>
          </div>
        </a>
      </div>
    </div>
    <span class="newsGradient"></span>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/navBarShow.js"></script>
</body>

</html>