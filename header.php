<header>
  <div class="contHeader">
    <a href="/">
      <img width="40" height="40" src="img/gualy icon app-01@2x.png" alt="G">
      <img src="img/Unión 1.svg" alt="Gualy">
    </a>

    <nav class="mainNav">
      <a class="navLink" href="como-funciona">Cómo funciona</a>
      <a class="navLink" href="afilia-tu-comercio">Tu negocio</a>
      <a class="navLink" href="contacto">Contacto y soporte</a>
      <div class="navAppCont">
        <a class="NavSpaceImg" href="https://play.google.com/store/apps/details?id=com.humbee.gualy&hl=es" target="_blank">
          <img src="img/Grupo 68.svg" alt="google-play">
        </a>
        <a class="NavSpaceImg" href="https://apps.apple.com/ve/app/gualy/id1463434706" target="_blank">
          <img src="img/Group 5.svg" alt="app-store">
        </a>
      </div>

      <a class="navLink signIn" href="https://app.gualy.com/registro">REGISTRO</a>
      <a class="pwaButton" href="https://app.gualy.com/"><img class="pwaButton" src="img/Grupo 179.png"></a>
    </nav>
    <button class="spanNav" id="navBarShow" onclick="showNavBar()">
      <img src="img/ic_menu_24px.svg">
    </button>
    <span class="navBar" id="navBarCont" style="display: none">
      <div class="navBarCont">
        <a class="navLink" href="como-funciona">Cómo funciona</a>
        <a class="navLink" href="afilia-tu-comercio">Tu negocio</a>
        <a class="navLink" href="contacto">Contacto y soporte</a>
        <div class="navAppCont">
          <a class="NavSpaceImg" href="https://play.google.com/store/apps/details?id=com.humbee.gualy&hl=es" target="_blank">
            <img src="img/Grupo 68.svg" alt="google-play">
          </a>
          <a class="NavSpaceImg" href="https://apps.apple.com/ve/app/gualy/id1463434706" target="_blank">
            <img src="img/Group 5.svg" alt="app-store">
          </a>
        </div>
        <a class="navLink signIn" href="https://app.gualy.com/registro">REGISTRO</a>
        <a class="pwaButton" href="https://app.gualy.com/"><img class="pwaButton" src="img/Grupo 179.png"></a>
      </div>
    </span>
  </div>
</header>