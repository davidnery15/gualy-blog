<?php
if (isset($_POST['submit1'])) {
  $name     = $_POST['name1'];
  $lastName = $_POST['lastName1'];
  $mail     = $_POST['mail1'];
  $text     = $_POST['message1'];

  if (empty($name) || empty($lastName) || empty($mail)) {
    echo '<script> alert("Algunos campos se encuentran vacíos"); location.href="../contacto"; </script>';
  } else {
    $to = 'soporte@gualy.com';
    $subject  = 'Contacto Gualy Web';
    $fromName = $name . " " . $lastName;
    $headers  = "From: $fromName" . " <" . $mail . ">";

    $message  = "Nombre: " . $name . " " . $lastName;
    $message .= "\nCorreo: " . $mail;
    if(!$text == "") {
      $message .= "\nMensaje: " . $text;
    }

    $mail = @mail($to, $subject, $message, $headers);

    if ($mail) {
      echo "<script>alert('Datos enviados, le responderemos lo más pronto posible.');location.href ='javascript:history.back()';</script>";
    } else {
      echo "<script>alert('Error al enviar los datos');location.href ='javascript:history.back()';</script>";
    }
  }
}
?>