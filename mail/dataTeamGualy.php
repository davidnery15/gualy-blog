<?php
if (isset($_POST['submit'])) {
  $name     = $_POST['name'];
  $lastName = $_POST['lastName'];
  $mail     = $_POST['mail'];
  $phone    = $_POST['phone'];
  $vacant   = $_POST['vacant'];
  $urlLink  = $_POST['urlLink'];
  $urlPort  = $_POST['urlPort'];

  if (empty($name) || empty($lastName) || empty($mail) || empty($phone) || empty($vacant) || empty($urlLink) || empty($urlPort)) {
    echo '<script> alert("Algunos campos se encuentran vacíos"); location.href="../teamGualy"; </script>';
  } else {
    $to = 'info@gualy.com';
    $subject  = 'Solicitud de Trabajo - Gualy Web';
    $fromName = $name . " " . $lastName;
    $headers  = "From: $fromName" . " <" . $mail . ">";

    $message  = "Nombre: " . $name . " " . $lastName;
    $message .= "\nCorreo: " . $mail;
    $message .= "\nTelefono: " . $phone;
    $message .= "\nSolicitante para: " . $vacant;
    $message .= "\nLinkedIn: " . $urlLink;
    $message .= "\nPortafolio: " . $urlPort;

    $mail = @mail($to, $subject, $message, $headers); 

    if ($mail) {
      echo "<script>alert('Datos enviados, le responderemos lo más pronto posible.');location.href ='javascript:history.back()';</script>";
    } else {
      echo "<script>alert('Error al enviar los datos');location.href ='javascript:history.back()';</script>";
    }
  }
}
?>