<?php
error_reporting(E_ERROR | E_PARSE);
if (isset($_POST['submit'])) {
  $nameCommerce   = $_POST['nameCommerce'];
  $rif            = $_POST['rif'];
  $description    = $_POST['description'];
  $category       = $_POST['category'];
  $account        = $_POST['account'];
  $checkCommerce  = $_POST['checkCommerce'];
  $fiscarlAddress = $_POST['fiscarlAddress'];
  $name           = $_POST['name'];
  $lastName       = $_POST['lastName'];
  $ciType         = $_POST['ciType'];
  $ci             = $_POST['ci'];
  $email          = $_POST['email'];
  $phone          = $_POST['phone'];
  $pass           = $_POST['pass'];
  $question       = $_POST['question'];
  $answer         = $_POST['answer'];

  if (
    empty($nameCommerce) ||
    empty($rif) ||
    empty($description) ||
    empty($category) ||
    empty($account) ||
    empty($checkCommerce) ||
    empty($name) ||
    empty($lastName) ||
    empty($ciType) ||
    empty($ci) ||
    empty($email) ||
    empty($phone)
    // empty($pass) ||
    // empty($question) ||
    // empty($answer)
  ) {
    echo '<script> alert("Algunos campos se encuentran vacíos"); location.href="../afilia-tu-comercio"; </script>';
  } else {

    $sCuerpo  = "Datos del Comercio";
    $sCuerpo .= "\nNombre: " . $nameCommerce;
    $sCuerpo .= "\nR.I.F.: " . $rif;
    $sCuerpo .= "\nDescripción: " . $description;
    $sCuerpo .= "\nCategoría: " . $category;
    $sCuerpo .= "\nCuenta Bancaria: " . $account;
    if ($checkCommerce == "1") {
      if ($fiscarlAddress == "") {
        echo '<script> alert("Algunos campos se encuentran vacíos"); location.href="../afilia-tu-comercio"; </script>';
      } else {
        $sCuerpo .= "\nDirección Fiscal: " . $fiscarlAddress;
      }
    } else {
      $sCuerpo .= "\nTipo de Comercio: Digital";
    }
    $sCuerpo .= "\n\nDatos de Contacto";
    $sCuerpo .= "\nNombre: " . $name . " " . $lastName;
    $sCuerpo .= "\nCedula: " . $ciType . " " . $ci;
    $sCuerpo .= "\nCorreo: " . $email;
    $sCuerpo .= "\nTelefono: " . $phone;
    $sCuerpo .= "\n\nDatos de Seguridad";
    $sCuerpo .= "\nContraseña: " . $pass;
    $sCuerpo .= "\nPregunta de seguridad: " . $question;
    $sCuerpo .= "\nRespuesta: " . $answer;

    // Recorremos los Ficheros
    $bHayFicheros = 0;
    foreach ($_FILES as $vAdjunto) {
      if ($bHayFicheros == 0) {
        $bHayFicheros = 1;

        // Cabeceras generales del mail
        $sSeparador  = uniqid("_Separador-de-datos_");
        $sCabeceras  = "MIME-version: 1.0\n";
        $sCabeceras .= "Content-type: multipart/mixed;";
        $sCabeceras .= "boundary=\"" . $sSeparador . "\"\n";

        // Cabeceras del texto
        $sCabeceraTexto  = "--" . $sSeparador . "\n";
        $sCabeceraTexto .= "Content-type: text/plain;charset=iso-8859-1\n";
        $sCabeceraTexto .= "Content-transfer-encoding: 7BIT\n\n";

        $sCuerpo = $sCabeceraTexto . $sCuerpo;
      }
      // Se añade el fichero
      if ($vAdjunto["size"] > 0) {
        $sAdjuntos  = "\n\n--" . $sSeparador . "\n";
        $sAdjuntos .= "Content-type: " . $vAdjunto["type"] . ";name=\"" . $vAdjunto["name"] . "\"\n";
        $sAdjuntos .= "Content-Transfer-Encoding: BASE64\n";
        $sAdjuntos .= "Content-disposition: attachment;filename=\"" . $vAdjunto["name"] . "\"\n\n";

        $oFichero = fopen($vAdjunto["tmp_name"], 'rb');
        $sContenido = fread($oFichero, filesize($vAdjunto["tmp_name"]));
        $sAdjuntos .= chunk_split(base64_encode($sContenido));
        fclose($oFichero);
      }
    }

    // Si hay ficheros se añaden al cuerpo
    if ($bHayFicheros)
      $sCuerpo .= $sAdjuntos . "\n\n--" . $sSeparador . "--\n";

    // Se añade la cabecera de destinatario
    if ($email) $sCabeceras .= "From:" . $email . "\n";

    $sPara = 'davidnery15@gmail.com';
    $sAsunto = 'Afiliación de Comercio Gualy';

    $mail = @mail($sPara, $sAsunto, $sCuerpo, $sCabeceras);

    if ($mail) {
      echo "<script>alert('Datos enviados, le responderemos lo más pronto posible.');location.href ='javascript:history.back()';</script>";
    } else {
      echo "<script>alert('Error al enviar los datos');location.href ='javascript:history.back()';</script>";
    }
  }
}
?>