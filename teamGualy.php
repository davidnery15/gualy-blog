<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/contact.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="howItWork">
    <p class="titleSection st">ÚNETE AL TEAM GUALY</p>
    <p class="subjectText stm">Completa nuestro formulario y da el primer paso para sumarte al lugar donde vuelan las ideas.</p>
    <form action="mail/dataTeamGualy" method="POST" id="teamGualyForm">
      <div class="inputCont">
        <input class="dateInput datInpSize" type="text" placeholder="Nombre" id="name" name="name">
        <input class="dateInput datInpSize" type="text" placeholder="Apellido" id="lastName" name="lastName">
      </div>
      <input class="subject datInpSize" type="text" placeholder="Correo" id="email" name="mail">
      <div class="inputCont">
        <input class="dateInput datInpSize" type="text" placeholder="Teléfono" id="phone" name="phone">
        <select class="selectTeam" id="vacant" name="vacant">
          <option value="">Vacante a la que aplicas</option>
          <option value="Desarrollador Android">Desarrollador Android</option>
          <option value="Desarrollador iOS">Desarrollador iOS</option>
          <option value="Desarrollador Frontend">Desarrollador Frontend</option>
          <option value="Desarrollador Backend">Desarrollador Backend</option>
        </select>
      </div>
      <input class="subject datInpSize" type="text" placeholder="URL de tu CV ó Perfil de LinkedIn" id="urlLink" name="urlLink">
      <input class="datInpSize quitMar" type="text" placeholder="URL de tu portafolio" id="urlPort" name="urlPort">
      <button class="submit" type="submit" name="submit">Enviar</button>
    </form>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/navBarShow.js"></script>
  <script src="js/validateTeamGualy.js"></script>
</body>

</html>