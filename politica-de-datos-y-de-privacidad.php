<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/footerLink.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="gualyPayment">
    <div class="gualyPaymentCont">
      <h2 class="titleSection titleSectionTerms">POLÍTICA DE DATOS Y DE PRIVACIDAD DE GUALY</h2>
      <p><strong>1. INTRODUCCIÓN.</strong></p>
      <p>Gualy Payment, C.A. (en lo sucesivo Gualy), es una empresa constituida bajo las leyes de Venezuela, en consecuencia, hemos creado la siguiente Política de datos y de privacidad para que usted sepa el tipo de información que recopilamos cuando visita nuestros sitios web, utiliza nuestros productos, servicios o apps (nuestro “Servicio”), o cuando accede y/o recurre a servicios de terceros que utilizan nuestra Interfaz de programación de aplicaciones (“API”), por qué recopilamos dicha información y cómo se utiliza.</p>
      <p>De manera periódica, podemos publicar cambios a esta Política de datos y de privacidad en esta página, por lo que confiamos en que revise su contenido con frecuencia. También podremos notificarle los cambios a través de correo electrónico o mediante otro canal de comunicación.</p>
      <p><strong>2. CÓMO RECOGEMOS INFORMACIÓN SOBRE USTED.</strong></p>
      <p>2.1 Recopilamos información sobre usted cuando se registra o utiliza nuestro Servicio. En el momento de registrarse, podemos solicitarle información detallada, incluyendo su nombre, apellido, cedula de identidad, pasaporte dirección de correo electrónico, dirección postal, número de teléfono, documento oficial de identidad u otra información. También podemos solicitar y recibir información sobre usted de otras fuentes, o utilizar dichas fuentes para confirmar la información que usted nos proporciona.</p>
      <p>2.2 Cuando visita nuestro sitio y/o utiliza nuestro Servicio, recogemos de manera automática la información que su ordenador, teléfono móvil u otro dispositivo nos envía. Esta información puede incluir su dirección IP, información de dispositivo (lo que incluye, entre otros, el identificador, nombre y tipo, sistema operativo, información de la red de telefonía móvil), información de registro web estándar, tal como el tipo de navegador y las páginas a las que accede en nuestro sitio.</p>
      <p>2.3 Cuando utiliza un dispositivo de localización con nuestro Servicio, podemos recoger datos de ubicación geográfica o utilizar diversos medios para determinar su ubicación, tales como datos de los sensores de su dispositivo que pueden, por ejemplo, proporcionar información sobre torres celulares o puntos de acceso wi-fi cercanos.</p>
      <p>2.4 Podemos registrar información usando “cookies”. Las cookies son pequeños ficheros de datos almacenados en su disco duro por un sitio web, que nos ayudan a mejorar nuestro sitio y Servicio y el uso que usted haga de ellos, permitiéndonos reconocer su navegador y capturar y recordar cierta información. Visite nuestra Política de Cookies para información adicional.</p>
      <p><strong>3. CÓMO UTILIZAMOS SU INFORMACIÓN PERSONAL.</strong></p>
      <p>3.1 Utilizamos su información personal para: (i) operar, mantener y mejorar nuestro Servicio; (ii) responder a comentarios y a preguntas, así como proporcionar asistencia a nuestros Miembros; (iii) enviar información, incluyendo confirmaciones, facturas, notas técnicas, actualizaciones, alertas de seguridad, así como mensajes administrativos y de asistencia; (iv) comunicar acerca de promociones, eventos y otras noticias sobre productos y servicios ofrecidos por nosotros o nuestros afiliados; (v) enlazar o combinar información de usuario con otros datos personales; (vi) proteger, investigar o disuadir contra actividades fraudulentas, no autorizadas o ilegales; y (vii) ofrecer y prestar el Servicio.</p>
      <p>3.2 Si usted se registra o utiliza nuestro Servicio, nosotros, o los afiliados, socios y/o proveedores que actúen en nuestro nombre, pueden recopilar los siguientes tipos de información:</p>
      <ul class="listNoOrder">
        <li class="questTextSpace">Información de contacto: su nombre, dirección, teléfono, dirección de correo electrónico;</li>
        <li class="questTextSpace">Información de verificación de identidad: es posible que le solicitemos información adicional que podemos utilizar y facilitar a los socios y/o proveedores que actúen en nuestro nombre para comprobar su identidad. Esta información puede incluir su fecha de nacimiento, número de identificación, una copia de su documento nacional de identificación u otros datos personales;</li>
        <li class="questTextSpace">Recopilación continua de información: cuando utiliza nuestro Servicio, recopilamos información sobre sus transacciones y/o otras actividades en nuestro sitio o Servicio y constantemente podemos recopilar información sobre su ordenador, dispositivo móvil u otro dispositivo de acceso para proteger el Servicio, a sus miembros y a nosotros mismos, para prevenir posibles fraudes y repararlos, controlar actividades irregulares en su cuenta e identificar software malicioso conocido u otras actividades que puedan dañarnos a nosotros o a nuestros miembros.</li>
      </ul>
      <p>3.3 También podemos recopilar información adicional que usted puede divulgar a nuestro equipo de soporte a miembros para resolver cualquier problema del que nos informe.</p>
      <p>3.4 En ningún caso almacenamos información de sus instrumentos bancarios o financieros cuando realiza operaciones con ellos a través de nuestra plataforma Cuando los emplea para agregar, trasferir o retirar fondos de su cuenta, no almacenamos esa información, la misma se emplea de manera encriptada. En este link puedes obtener más información sobre <a href="seguridad">nuestros procedimientos para garantizar la protección de datos personales de clientes.</a></p>
      <p><strong>4. CÓMO PROTEGEMOS SU INFORMACIÓN PERSONAL.</strong></p>
      <p>4.1 Utilizamos el término “información personal” para describir la información que puede estar asociada con una persona en concreto y que puede utilizarse para identificar a esa persona. No consideramos que información personal incluya aquellos datos que hayan sido anonimizados y que, por tanto, no identifican a un usuario concreto. Tomamos medidas cuidadosas, que revisamos periódicamente, como se describe aquí, para proteger su información personal.</p>
      <p>4.2 Almacenamos y procesamos su información personal y relativa a transacciones en nuestros servidores de Estados Unidos y en todos aquellos lugares en los que se encuentran nuestras filiales, socios o proveedores, y protegemos dicha información mediante salvaguardas físicas, electrónicas y procedimentales, que incorporan tecnologías de seguridad probadas, en cumplimiento de la legislación aplicable. Podemos utilizar medidas de seguridad informática tales como firewalls y encriptación de datos, hacer cumplir controles de acceso físico, y autorizar el acceso a su información personal únicamente a aquellas personas que necesitan dicho acceso para llevar a cabo su trabajo. Aquellas personas que tienen acceso a su información personal pasan un examen detallado, son periódicamente reevaluados y están obligados a mantener la confidencialidad de toda la información personal de los miembros. Los riesgos de seguridad existen, y ninguna organización puede prometerle lo contrario. Los agentes con malas intenciones pueden derrotar incluso las medidas de seguridad más cuidadosamente aplicadas.</p>
      <p>4.3 Almacenamos información personal de forma segura y la conservaremos durante el tiempo que sea necesario para mantener el Servicio, cumplir con nuestras obligaciones legales y/o resolver disputas.</p>
      <p><strong>5. DIVULGACIÓN DE SU INFORMACIÓN PERSONAL.</strong></p>
      <p>5.1 No vendemos, comerciamos ni transferimos su información personal a terceros. Esto no incluye a terceros que nos asisten en el funcionamiento de nuestro Servicio, en la realización de nuestro negocio o en la asistencia a nuestros miembros, siempre que estos terceros se comprometan a mantener dicha información confidencial y segura, en las mismas condiciones y niveles de protección que le ofrecemos como miembro. Nuestros acuerdos con terceros que tienen acceso a su información personal generalmente les obligan a mantener su información personal segura y a borrarla cuando ya no sea necesario acceder a ella. De manera periódica, revisamos las prácticas de seguridad y confidencialidad de terceros a los que confiamos su información personal. También podemos divulgar su información a las autoridades cuando consideremos que es lo apropiado para cumplir con la ley, cumplir nuestros términos y políticas, o proteger los derechos, propiedad o seguridad de Gualy, nuestros miembros u otros.</p>
      <p>5.2 Podemos compartir su información personal del modo siguiente:</p>
      <ul class="listNoOrder">
        <li class="questTextSpace">Con su consentimiento;</li>
        <li class="questTextSpace">para cumplir con las leyes aplicables;</li>
        <li class="questTextSpace">para responder a solicitudes y procesos legales;</li>
        <li class="questTextSpace">para proteger nuestros derechos, los derechos de nuestros afiliados, socios o proveedores, los derechos de otros miembros y otros. Esto incluye, sin limitarse a ello, la divulgación que podamos necesitar para hacer cumplir nuestros acuerdos, términos y políticas;</li>
        <li class="questTextSpace">En caso de emergencia. Esto incluye la protección de la seguridad de nuestros empleados y agentes, nuestros miembros u otros;</li>
        <li class="questTextSpace">A ciertos empleados, afiliados y proveedores, cuando sea necesario para su trabajo; y</li>
        <li class="questTextSpace">Si procede, con entidades con las que nos fusionemos o que nos adquieran. (Si se produjera este caso, exigiríamos que la nueva entidad combinada siguiera esta Política respecto de su información personal y le informaríamos a usted de cualquier cambio material).</li>
      </ul>
      <p><strong>6. COMUNICACIONES DE PROMOCIÓN Y PUBLICIDAD.</strong></p>
      <p>Nuestros correos electrónicos de promoción y publicidad le informan sobre cómo desactivar la recepción de más correos de ese tipo. Si decide desactivarlos, podremos seguir enviándole correos no relacionados con marketing y otras comunicaciones electrónicas. Los correos no relacionados con promoción y publicidad incluyen correos electrónicos sobre su cuenta, transacciones y otros aspectos de nuestro sitio o Servicios. Usted puede enviar solicitudes relativas a su información personal a nuestros equipos de atención al cliente, mencionados más abajo, y pedir el cambio de sus elecciones de contacto, desactivar nuestro intercambio de información con otros, y actualizar sus datos personales.</p>
      <p><strong>7. INFORMACIÓN DE CONTACTO</strong></p>
      <p>Si tiene alguna pregunta respecto de esta Política de datos y de privacidad, puede ponerse en contacto con nosotros utilizando correo electrónico soporte@gualy.com.</p>
    </div>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/navBarShow.js"></script>
</body>

</html>