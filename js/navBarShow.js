
document.onclick = function (e) {
    e = e || event;
    var target = e.target || e.srcElement;
    var navBarShow = document.getElementById('navBarShow');
    var navBarCont = document.getElementById('navBarCont');

    do {
        if (navBarCont == target || navBarShow == target) {
        return;
        }
    target = target.parentNode;
    } while (target)
    navBarCont.style.display = "none";
}

function showNavBar() {
    var x = document.getElementById("navBarCont");
    if (x.style.display === "none") {
        x.style.display = "flex";
    } else {
        x.style.display = "none";
    }
}