(function () {
  var form = document.getElementById('contactForm1');
  var elements = form.elements;

  var nameValidation = function(e) {
    var inputName = document.getElementById('name1');
    var name = elements.name1.value;
    if (name === "") {
      inputName.className += ' redBorder';
      e.preventDefault();
    }else{
      var regexp = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
      if (!regexp.test(name)) {
        inputName.className += ' redBorder';
        e.preventDefault();
      }else{
        inputName.className = 'dateInput';
      }
    }
  };

  var lastNameValidation = function(e) {
    var inputLastName = document.getElementById('lastName1');
    var lastName = elements.lastName1.value;
    if (lastName === "") {
      inputLastName.className += ' redBorder';
      e.preventDefault();
    }else{
      var regexp = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
      if (!regexp.test(lastName)) {
        inputLastName.className += ' redBorder';
        e.preventDefault();
      }else{
        inputLastName.className = 'dateInput';
      }
    }
  };

  var emailValidation = function(e) {
    var inputEmail = document.getElementById('email1');
    var email = elements.email1.value;
    if (email === "") {
      inputEmail.className += ' redBorder';
      e.preventDefault();
    }else{
      var regexp = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$/i;
      if (!regexp.test(email)) {
        inputEmail.className += ' redBorder';
        e.preventDefault();
      }else{
        inputEmail.className = 'subject';
      }
    }
  };

  var validation = function(e) {
    nameValidation(e);
    lastNameValidation(e);
    emailValidation(e);
  }

  form.addEventListener('submit', validation);
}());