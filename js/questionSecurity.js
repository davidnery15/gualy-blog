
document.onclick = function (e) {
    /* Header */
    e = e || event;
    var target = e.target || e.srcElement;
    var navBarShow = document.getElementById('navBarShow');
    var navBarCont = document.getElementById('navBarCont');

    do {
        if (navBarCont == target || navBarShow == target) {
        return;
        }
    target = target.parentNode;
    } while (target)
    navBarCont.style.display = "none";

    /* Cómo protegemos tu información financiera */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow = document.getElementById('questionButtonShow');
    var questionShowHide = document.getElementById('questionShowHide');
    var img = document.getElementById("questionButtonImg");

    do {
        if (questionShowHide == target || questionButtonShow == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Cifrado y almacenamiento */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow2 = document.getElementById('questionButtonShow2');
    var questionShowHide2 = document.getElementById('questionShowHide2');
    var img = document.getElementById("questionButtonImg2");

    do {
        if (questionShowHide2 == target || questionButtonShow2 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide2.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Protección de tu cuenta */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow3 = document.getElementById('questionButtonShow3');
    var questionShowHide3 = document.getElementById('questionShowHide3');
    var img = document.getElementById("questionButtonImg3");

    do {
        if (questionShowHide3 == target || questionButtonShow3 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide3.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Protege tu cuenta y transacciones */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow4 = document.getElementById('questionButtonShow4');
    var questionShowHide4 = document.getElementById('questionShowHide4');
    var img = document.getElementById("questionButtonImg4");

    do {
        if (questionShowHide4 == target || questionButtonShow4 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide4.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Soporte y Seguridad */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow5 = document.getElementById('questionButtonShow5');
    var questionShowHide5 = document.getElementById('questionShowHide5');
    var img = document.getElementById("questionButtonImg5");

    do {
        if (questionShowHide5 == target || questionButtonShow5 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide5.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");
}

/* Header */
function showNavBar() {
    var x = document.getElementById("navBarCont");
    if (x.style.display === "none") {
        x.style.display = "flex";
    } else {
        x.style.display = "none";
    }
}

/* Cómo protegemos tu información financiera */
function questionButtonShow() {
    var x = document.getElementById("questionShowHide");
    var img = document.getElementById("questionButtonImg");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Cifrado y almacenamiento */
function questionButtonShow2() {
    var x = document.getElementById("questionShowHide2");
    var img = document.getElementById("questionButtonImg2");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Protección de tu cuenta */
function questionButtonShow3() {
    var x = document.getElementById("questionShowHide3");
    var img = document.getElementById("questionButtonImg3");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Protege tu cuenta y transacciones */
function questionButtonShow4() {
    var x = document.getElementById("questionShowHide4");
    var img = document.getElementById("questionButtonImg4");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Soporte y Seguridad */
function questionButtonShow5() {
    var x = document.getElementById("questionShowHide5");
    var img = document.getElementById("questionButtonImg5");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}