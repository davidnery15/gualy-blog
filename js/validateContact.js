(function () {
  var form = document.getElementById('contactForm');
  var elements = form.elements;

  var nameValidation = function(e) {
    var inputName = document.getElementById('name');
    var name = elements.name.value;
    if (name === "") {
      inputName.className += ' redBorder';
      e.preventDefault();
    }else{
      var regexp = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
      if (!regexp.test(name)) {
        inputName.className += ' redBorder';
        e.preventDefault();
      }else{
        inputName.className = 'dateInput';
      }
    }
  };

  var lastNameValidation = function(e) {
    var inputLastName = document.getElementById('lastName');
    var lastName = elements.lastName.value;
    if (lastName === "") {
      inputLastName.className += ' redBorder';
      e.preventDefault();
    }else{
      var regexp = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
      if (!regexp.test(lastName)) {
        inputLastName.className += ' redBorder';
        e.preventDefault();
      }else{
        inputLastName.className = 'dateInput';
      }
    }
  };

  var emailValidation = function(e) {
    var inputEmail = document.getElementById('email');
    var email = elements.email.value;
    if (email === "") {
      inputEmail.className += ' redBorder';
      e.preventDefault();
    }else{
      var regexp = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$/i;
      if (!regexp.test(email)) {
        inputEmail.className += ' redBorder';
        e.preventDefault();
      }else{
        inputEmail.className = 'subject';
      }
    }
  };

  var validation = function(e) {
    nameValidation(e);
    lastNameValidation(e);
    emailValidation(e);
  }

  form.addEventListener('submit', validation);
}());