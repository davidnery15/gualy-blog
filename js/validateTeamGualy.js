(function () {
  var form = document.getElementById('teamGualyForm');
  var elements = form.elements;

  var nameValidation = function (e) {
    var inputName = document.getElementById('name');
    var name = elements.name.value;
    if (name === "") {
      inputName.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      var regexp = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
      if (!regexp.test(name)) {
        inputName.className += ' redBorderButtom';
        e.preventDefault();
      } else {
        inputName.className = 'dateInput datInpSize';
      }
    }
  };

  var lastNameValidation = function (e) {
    var inputLastName = document.getElementById('lastName');
    var lastName = elements.lastName.value;
    if (lastName === "") {
      inputLastName.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      var regexp = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
      if (!regexp.test(lastName)) {
        inputLastName.className += ' redBorderButtom';
        e.preventDefault();
      } else {
        inputLastName.className = 'dateInput datInpSize';
      }
    }
  };

  var emailValidation = function (e) {
    var inputEmail = document.getElementById('email');
    var email = elements.email.value;
    if (email === "") {
      inputEmail.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      var regexp = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$/i;
      if (!regexp.test(email)) {
        inputEmail.className += ' redBorderButtom';
        e.preventDefault();
      } else {
        inputEmail.className = 'subject datInpSize';
      }
    }
  };

  var phoneValidation = function (e) {
    var inputPhone = document.getElementById('phone');
    var phone = elements.phone.value;
    if (phone === "") {
      inputPhone.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      var regexp = /^[0123456789]{7,}$/i;
      if (!regexp.test(phone)) {
        inputPhone.className += ' redBorderButtom';
        e.preventDefault();
      } else {
        inputPhone.className = 'dateInput datInpSize';
      }
    }
  };

  var vacantValidation = function (e) {
    var inputVacant = document.getElementById('vacant');
    var vacant = elements.vacant.value;
    if (vacant === "") {
      inputVacant.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      inputVacant.className = 'selectTeam';
    }
  };

  var urlValidation = function (e) {
    var inputUrlLink = document.getElementById('urlLink');
    var urlLink = elements.urlLink.value;
    var inputUrlPort = document.getElementById('urlPort');
    var urlPort = elements.urlPort.value;
    var regexp = /^https?:\/\/[\w\-]+(\.[\w\-]+)+[/#?]?.*$/i;
    
    if (urlLink === "") {
      inputUrlLink.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      if (!regexp.test(urlLink)) {
        inputUrlLink.className += ' redBorderButtom';
        e.preventDefault();
      } else {
        inputUrlLink.className = 'subject datInpSize';
      }
    }

    if (urlPort === "") {
      inputUrlPort.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      if (!regexp.test(urlPort)) {
        inputUrlPort.className += ' redBorderButtom';
        e.preventDefault();
      } else {
        inputUrlPort.className = 'datInpSize quitMar';
      }
    }
  };

var validation = function (e) {
  nameValidation(e);
  lastNameValidation(e);
  emailValidation(e);
  phoneValidation(e);
  vacantValidation(e);
  urlValidation(e);
}

form.addEventListener('submit', validation);
}());