
document.onclick = function (e) {
    /* Header */
    e = e || event;
    var target = e.target || e.srcElement;
    var navBarShow = document.getElementById('navBarShow');
    var navBarCont = document.getElementById('navBarCont');

    do {
        if (navBarCont == target || navBarShow == target) {
        return;
        }
    target = target.parentNode;
    } while (target)
    navBarCont.style.display = "none";

    /* ¿Quiénes pueden usar gualy? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow = document.getElementById('questionButtonShow');
    var questionShowHide = document.getElementById('questionShowHide');
    var img = document.getElementById("questionButtonImg");

    do {
        if (questionShowHide == target || questionButtonShow == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Qué debo hacer para tener una cuenta en gualy? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow2 = document.getElementById('questionButtonShow2');
    var questionShowHide2 = document.getElementById('questionShowHide2');
    var img = document.getElementById("questionButtonImg2");

    do {
        if (questionShowHide2 == target || questionButtonShow2 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide2.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cómo agrego dinero en gualy? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow3 = document.getElementById('questionButtonShow3');
    var questionShowHide3 = document.getElementById('questionShowHide3');
    var img = document.getElementById("questionButtonImg3");

    do {
        if (questionShowHide3 == target || questionButtonShow3 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide3.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cómo envío y solicito un pago? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow4 = document.getElementById('questionButtonShow4');
    var questionShowHide4 = document.getElementById('questionShowHide4');
    var img = document.getElementById("questionButtonImg4");

    do {
        if (questionShowHide4 == target || questionButtonShow4 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide4.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cuánto tiempo toma pagar con gualy? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow5 = document.getElementById('questionButtonShow5');
    var questionShowHide5 = document.getElementById('questionShowHide5');
    var img = document.getElementById("questionButtonImg5");

    do {
        if (questionShowHide5 == target || questionButtonShow5 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide5.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Si recibo un pago, ¿en cuánto tiempo puedo usar el dinero? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow6 = document.getElementById('questionButtonShow6');
    var questionShowHide6 = document.getElementById('questionShowHide6');
    var img = document.getElementById("questionButtonImg6");

    do {
        if (questionShowHide6 == target || questionButtonShow6 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide6.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cómo puedo ver los detalles de las transacciones? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow7 = document.getElementById('questionButtonShow7');
    var questionShowHide7 = document.getElementById('questionShowHide7');
    var img = document.getElementById("questionButtonImg7");

    do {
        if (questionShowHide7 == target || questionButtonShow7 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide7.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Qué tarifa cobra gualy por recibir dinero? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow8 = document.getElementById('questionButtonShow8');
    var questionShowHide8 = document.getElementById('questionShowHide8');
    var img = document.getElementById("questionButtonImg8");

    do {
        if (questionShowHide8 == target || questionButtonShow8 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide8.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cómo se puede retirar el dinero? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow9 = document.getElementById('questionButtonShow9');
    var questionShowHide9 = document.getElementById('questionShowHide9');
    var img = document.getElementById("questionButtonImg9");

    do {
        if (questionShowHide9 == target || questionButtonShow9 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide9.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Aceptan moneda extranjera? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow10 = document.getElementById('questionButtonShow10');
    var questionShowHide10 = document.getElementById('questionShowHide10');
    var img = document.getElementById("questionButtonImg10");

    do {
        if (questionShowHide10 == target || questionButtonShow10 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide10.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cómo puedo ver o editar la información de mi perfil? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow11 = document.getElementById('questionButtonShow11');
    var questionShowHide11 = document.getElementById('questionShowHide11');
    var img = document.getElementById("questionButtonImg11");

    do {
        if (questionShowHide11 == target || questionButtonShow11 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide11.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cómo puedo restablecer mi contraseña? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow12 = document.getElementById('questionButtonShow12');
    var questionShowHide12 = document.getElementById('questionShowHide12');
    var img = document.getElementById("questionButtonImg12");

    do {
        if (questionShowHide12 == target || questionButtonShow12 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide12.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cómo puedo solicitar ayuda o realizar un reclamo? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow13 = document.getElementById('questionButtonShow13');
    var questionShowHide13 = document.getElementById('questionShowHide13');
    var img = document.getElementById("questionButtonImg13");

    do {
        if (questionShowHide13 == target || questionButtonShow13 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide13.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* ¿Cómo verifico mi usuario? */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow14 = document.getElementById('questionButtonShow14');
    var questionShowHide14 = document.getElementById('questionShowHide14');
    var img = document.getElementById("questionButtonImg14");

    do {
        if (questionShowHide14 == target || questionButtonShow14 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide14.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");
}

/* Header */
function showNavBar() {
    var x = document.getElementById("navBarCont");
    if (x.style.display === "none") {
        x.style.display = "flex";
    } else {
        x.style.display = "none";
    }
}

/* ¿Quiénes pueden usar gualy? */
function questionButtonShow() {
    var x = document.getElementById("questionShowHide");
    var img = document.getElementById("questionButtonImg");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Qué debo hacer para tener una cuenta en gualy? */
function questionButtonShow2() {
    var x = document.getElementById("questionShowHide2");
    var img = document.getElementById("questionButtonImg2");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cómo agrego dinero en gualy? */
function questionButtonShow3() {
    var x = document.getElementById("questionShowHide3");
    var img = document.getElementById("questionButtonImg3");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cómo envío y solicito un pago? */
function questionButtonShow4() {
    var x = document.getElementById("questionShowHide4");
    var img = document.getElementById("questionButtonImg4");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cuánto tiempo toma pagar con gualy? */
function questionButtonShow5() {
    var x = document.getElementById("questionShowHide5");
    var img = document.getElementById("questionButtonImg5");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Si recibo un pago, ¿en cuánto tiempo puedo usar el dinero? */
function questionButtonShow6() {
    var x = document.getElementById("questionShowHide6");
    var img = document.getElementById("questionButtonImg6");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cómo puedo ver los detalles de las transacciones? */
function questionButtonShow7() {
    var x = document.getElementById("questionShowHide7");
    var img = document.getElementById("questionButtonImg7");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Qué tarifa cobra gualy por recibir dinero? */
function questionButtonShow8() {
    var x = document.getElementById("questionShowHide8");
    var img = document.getElementById("questionButtonImg8");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cómo se puede retirar el dinero? */
function questionButtonShow9() {
    var x = document.getElementById("questionShowHide9");
    var img = document.getElementById("questionButtonImg9");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Aceptan moneda extranjera? */
function questionButtonShow10() {
    var x = document.getElementById("questionShowHide10");
    var img = document.getElementById("questionButtonImg10");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cómo puedo ver o editar la información de mi perfil? */
function questionButtonShow11() {
    var x = document.getElementById("questionShowHide11");
    var img = document.getElementById("questionButtonImg11");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cómo puedo restablecer mi contraseña? */
function questionButtonShow12() {
    var x = document.getElementById("questionShowHide12");
    var img = document.getElementById("questionButtonImg12");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cómo puedo solicitar ayuda o realizar un reclamo? */
function questionButtonShow13() {
    var x = document.getElementById("questionShowHide13");
    var img = document.getElementById("questionButtonImg13");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* ¿Cómo verifico mi usuario? */
function questionButtonShow14() {
    var x = document.getElementById("questionShowHide14");
    var img = document.getElementById("questionButtonImg14");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}