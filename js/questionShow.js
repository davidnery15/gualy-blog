
document.onclick = function (e) {

    /* Header */
    e = e || event;
    var target = e.target || e.srcElement;
    var navBarShow = document.getElementById('navBarShow');
    var navBarCont = document.getElementById('navBarCont');

    do {
        if (navBarCont == target || navBarShow == target) {
        return;
        }
    target = target.parentNode;
    } while (target)
    navBarCont.style.display = "none";

    /* Enviar pago */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow = document.getElementById('questionButtonShow');
    var questionShowHide = document.getElementById('questionShowHide');
    var img = document.getElementById("questionButtonImg");

    do {
        if (questionShowHide == target || questionButtonShow == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Solicitar pago */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow2 = document.getElementById('questionButtonShow2');
    var questionShowHide2 = document.getElementById('questionShowHide2');
    var img = document.getElementById("questionButtonImg2");

    do {
        if (questionShowHide2 == target || questionButtonShow2 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide2.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Historial */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow3 = document.getElementById('questionButtonShow3');
    var questionShowHide3 = document.getElementById('questionShowHide3');
    var img = document.getElementById("questionButtonImg3");

    do {
        if (questionShowHide3 == target || questionButtonShow3 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide3.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Añadir saldo */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow4 = document.getElementById('questionButtonShow4');
    var questionShowHide4 = document.getElementById('questionShowHide4');
    var img = document.getElementById("questionButtonImg4");

    do {
        if (questionShowHide4 == target || questionButtonShow4 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide4.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Retirar saldo */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow5 = document.getElementById('questionButtonShow5');
    var questionShowHide5 = document.getElementById('questionShowHide5');
    var img = document.getElementById("questionButtonImg5");

    do {
        if (questionShowHide5 == target || questionButtonShow5 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide5.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Compartir tu codigo */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow6 = document.getElementById('questionButtonShow6');
    var questionShowHide6 = document.getElementById('questionShowHide6');
    var img = document.getElementById("questionButtonImg6");

    do {
        if (questionShowHide6 == target || questionButtonShow6 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide6.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Ver los comercios */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow7 = document.getElementById('questionButtonShow7');
    var questionShowHide7 = document.getElementById('questionShowHide7');
    var img = document.getElementById("questionButtonImg7");

    do {
        if (questionShowHide7 == target || questionButtonShow7 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide7.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Directorio */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow8 = document.getElementById('questionButtonShow8');
    var questionShowHide8 = document.getElementById('questionShowHide8');
    var img = document.getElementById("questionButtonImg8");

    do {
        if (questionShowHide8 == target || questionButtonShow8 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide8.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Registro */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow9 = document.getElementById('questionButtonShow9');
    var questionShowHide9 = document.getElementById('questionShowHide9');
    var img = document.getElementById("questionButtonImg9");

    do {
        if (questionShowHide9 == target || questionButtonShow9 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide9.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");

    /* Agregar y eliminar cuentas bancarias */
    e = e || event;
    var target = e.target || e.srcElement;
    var questionButtonShow10 = document.getElementById('questionButtonShow10');
    var questionShowHide10 = document.getElementById('questionShowHide10');
    var img = document.getElementById("questionButtonImg10");

    do {
        if (questionShowHide10 == target || questionButtonShow10 == target) {
            return;
        }
    target = target.parentNode;
    } while (target)
    questionShowHide10.style.display = "none";
    img.setAttribute("src", "img/Unión 5.svg");
}

/* Header */
function showNavBar() {
    var x = document.getElementById("navBarCont");
    if (x.style.display === "none") {
        x.style.display = "flex";
    } else {
        x.style.display = "none";
    }
}

/* Enviar pago */
function questionButtonShow() {
    var x = document.getElementById("questionShowHide");
    var img = document.getElementById("questionButtonImg");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Solicitar pago */
function questionButtonShow2() {
    var x = document.getElementById("questionShowHide2");
    var img = document.getElementById("questionButtonImg2");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Historial */
function questionButtonShow3() {
    var x = document.getElementById("questionShowHide3");
    var img = document.getElementById("questionButtonImg3");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Añadir saldo */
function questionButtonShow4() {
    var x = document.getElementById("questionShowHide4");
    var img = document.getElementById("questionButtonImg4");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Retirar saldo */
function questionButtonShow5() {
    var x = document.getElementById("questionShowHide5");
    var img = document.getElementById("questionButtonImg5");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Compartir tu codigo */
function questionButtonShow6() {
    var x = document.getElementById("questionShowHide6");
    var img = document.getElementById("questionButtonImg6");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Ver los comercios */
function questionButtonShow7() {
    var x = document.getElementById("questionShowHide7");
    var img = document.getElementById("questionButtonImg7");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Directorio */
function questionButtonShow8() {
    var x = document.getElementById("questionShowHide8");
    var img = document.getElementById("questionButtonImg8");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Registro */
function questionButtonShow9() {
    var x = document.getElementById("questionShowHide9");
    var img = document.getElementById("questionButtonImg9");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}

/* Agregar y elimnar cuentas bancarias */
function questionButtonShow10() {
    var x = document.getElementById("questionShowHide10");
    var img = document.getElementById("questionButtonImg10");
    if (x.style.display === "none") {
        img.setAttribute("src", "img/Trazado 9.svg");
        x.style.display = "flex";
    } else {
        x.style.display = "none";
        img.setAttribute("src", "img/Unión 5.svg");
    }
}