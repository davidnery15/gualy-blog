
var button = document.getElementById("showHidePass");
button.addEventListener("click", function (e) {
    e.preventDefault();
});

function showPassword() {
    var pass = document.getElementById("pass");
    var img = document.getElementById("showHidePassImg");
    var action = pass.getAttribute("action");

    if (action === "show") {
        pass.setAttribute("type", "text");
        pass.setAttribute("action", "hide");
        img.setAttribute("src", "img/ic_visibility_off_24px.svg");
    }
    if (action === "hide") {
        pass.setAttribute("type", "password");
        pass.setAttribute("action", "show");
        img.setAttribute("src", "img/visibilityOn.svg");
    }
}