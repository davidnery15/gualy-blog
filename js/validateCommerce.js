(function () {
  var form = document.getElementById('businessForm');
  var elements = form.elements;

  var commercePictureValidation = function (e) {
    var inputName = document.getElementById('commercePicture');
    var name = elements.commercePictureInput.value;
    if (name == "") {
      inputName.className = 'commercePicBorderRed';
      e.preventDefault();
    }else{
      inputName.className = 'businessCommercePicbutton';
    }
  };

  var nameCommerceValidation = function (e) {
    var inputName = document.getElementById('nameCommerce');
    var name = elements.nameCommerce.value;
    if (name == "") {
      inputName.className += ' redBorderButtom';
      e.preventDefault();
    }else{
      inputName.className = 'nameCommerce';
    }
  };

  var rifValidation = function (e) {
    var inputEmail = document.getElementById('rif');
    var email = elements.rif.value;
    if (email === "") {
      inputEmail.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      var regexp = /^[A-Za-z-.,0-9áéíóúÁÉÍÓÚ ]{9,}$/i;
      if (!regexp.test(email)) {
        inputEmail.className += ' redBorderButtom';
        e.preventDefault();
      } else {
        inputEmail.className = 'rifCommerce';
      }
    }
  };

  var descriptionValidation = function (e) {
    var inputEmail = document.getElementById('description');
    var email = elements.description.value;
    if (email === "") {
      inputEmail.className = 'redBorderButtom';
      e.preventDefault();
    } else {
      inputEmail.className = '';
    }
  };

  var categoryValidation = function (e) {
    var inputEmail = document.getElementById('category');
    var email = elements.category.value;
    if (email === "") {
      inputEmail.className = 'redBorderButtom';
      e.preventDefault();
    } else {
      inputEmail.className = '';
    }
  };

  var accountValidation = function (e) {
    var inputEmail = document.getElementById('account');
    var email = elements.account.value;
    if (email === "") {
      inputEmail.className = 'redBorderButtom';
      e.preventDefault();
    } else {
      var regexp = /^[0123456879 ]{20,25}$/i;
      if (!regexp.test(email)) {
        inputEmail.className = 'redBorderButtom';
        e.preventDefault();
      } else {
        inputEmail.className = '';
      }
    }
  };

  var addressValidation = function (e) {
    var inputEmail = document.getElementById('fiscarlAddress');
    var email = elements.fiscarlAddress.value;
    if (email === "") {
      inputEmail.className += ' redBorderButtom';
    } else {
      var regexp = /^[A-Za-z-.,0-9áéíóúÁÉÍÓÚ ]*$/i;
      if (!regexp.test(email)) {
        inputEmail.className += ' redBorderButtom';
      } else {
        inputEmail.className = 'commerceInputSize';
      }
    }
  };

  var nameValidation = function(e) {
    var inputName = document.getElementById('name');
    var name = elements.name.value;
    if (name === "") {
      inputName.className = 'redBorderButtom';
      e.preventDefault();
    }else{
      var regexp = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
      if (!regexp.test(name)) {
        inputName.className = 'redBorderButtom';
        e.preventDefault();
      }else{
        inputName.className = '';
      }
    }
  };

  var lastNameValidation = function(e) {
    var inputLastName = document.getElementById('lastName');
    var lastName = elements.lastName.value;
    if (lastName === "") {
      inputLastName.className = 'redBorderButtom';
      e.preventDefault();
    }else{
      var regexp = /^[A-Za-zñÑÁÉÍÓÚáéíóúÿ]{2,}$/i;
      if (!regexp.test(lastName)) {
        inputLastName.className = 'redBorderButtom';
        e.preventDefault();
      }else{
        inputLastName.className = '';
      }
    }
  };

  var ciValidation = function (e) {
    var inputEmail = document.getElementById('ci');
    var email = elements.ci.value;
    if (email === "") {
      inputEmail.className = 'redBorderButtom';
      e.preventDefault();
    } else {
      var regexp = /^[0123456879]{6,8}$/i;
      if (!regexp.test(email)) {
        inputEmail.className = 'redBorderButtom';
        e.preventDefault();
      } else {
        inputEmail.className = '';
      }
    }
  };

  var emailValidation = function(e) {
    var inputEmail = document.getElementById('email');
    var email = elements.email.value;
    if (email === "") {
      inputEmail.className += ' redBorderButtom';
      e.preventDefault();
    }else{
      var regexp = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,5}$/i;
      if (!regexp.test(email)) {
        inputEmail.className += ' redBorderButtom';
        e.preventDefault();
      }else{
        inputEmail.className = 'contactInputEmail';
      }
    }
  };

  var phoneValidation = function (e) {
    var inputPhone = document.getElementById('phone');
    var phone = elements.phone.value;
    if (phone === "") {
      inputPhone.className += ' redBorderButtom';
      e.preventDefault();
    } else {
      var regexp = /^[0123456789]{11,}$/i;
      if (!regexp.test(phone)) {
        inputPhone.className += ' redBorderButtom';
        e.preventDefault();
      } else {
        inputPhone.className = 'dateInput datInpSize';
      }
    }
  };

  var passValidation = function (e) {
    var inputPhone = document.getElementById('pass');
    var pass = elements.pass.value;
    if (pass === "") {
      inputPhone.className = 'redBorderButtom';
      e.preventDefault();
    }else{
      inputPhone.className = '';
    }
  };

  var questionValidation = function (e) {
    var inputPhone = document.getElementById('question');
    var pass = elements.question.value;
    if (pass === "") {
      inputPhone.className = 'redBorderButtom';
      e.preventDefault();
    }else{
      inputPhone.className = '';
    }
  };

  var answerValidation = function (e) {
    var inputPhone = document.getElementById('answer');
    var pass = elements.answer.value;
    if (pass === "") {
      inputPhone.className = 'redBorderButtom';
      e.preventDefault();
    }else{
      inputPhone.className = '';
    }
  };

  var validation = function (e) {
    commercePictureValidation(e);
    nameCommerceValidation(e);
    rifValidation(e);
    descriptionValidation(e);
    categoryValidation(e);
    accountValidation(e);
    addressValidation(e);
    nameValidation(e);
    lastNameValidation(e);
    ciValidation(e);
    emailValidation(e);
    phoneValidation(e);
    passValidation(e);
    questionValidation(e);
    answerValidation(e);
  }

  form.addEventListener('submit', validation);
}());

var fiscarlAddress = document.getElementById('fiscarlAddress');
var iframeMaps = document.getElementById('iframeMaps');

function disableInputAddress() {
  fiscarlAddress.className += ' hideDisplay';
  iframeMaps.className += ' hideDisplay';
}

function enableInputAddress() {
  fiscarlAddress.className = 'commerceInputSize';
  iframeMaps.className = 'iframe';
}