<footer>
  <div class="mainFooter">
    <img src="img/Grupo 92.png" width="50" height="43" alt="Gualy">
    <div class="footerMobile">
      <a href="https://play.google.com/store/apps/details?id=com.humbee.gualy&hl=es">
        <img src="img/Grupo 68.svg" alt="google-play">
      </a>
      <a href="https://apps.apple.com/ve/app/gualy/id1463434706">
        <img src="img/Group 5.svg" alt="app-store">
      </a>
    </div>
    <div class="footerLinkCont">
      <div class="footerLinkContList">
        <p class="footerLinkTitle">CONÓCENOS</p>
        <a href="gualy-payment">Gualy payment</a>
        <a href="blog">Blog</a> 
        <a href="seguridad">Seguridad</a>
        <a href="teamGualy">Únete al team gualy</a>
      </div>
      <div class="footerLinkContList">
        <p class="footerLinkTitle">Políticas - T&C</p>
        <a href="politica-de-datos-y-de-privacidad">Política de privacidad</a>
        <a href="terminos-y-condiciones">Términos y condiciones</a>
      </div>
      <div class="footerLinkContList">
        <p class="footerLinkTitle">AFILIACIONES</p>
        <a href="https://connectamericas.com/es/company/gualy-payment" target="_blank">
          <img src="img/verifiedbadge.png" alt="connect-americas">
        </a>
        <a href="https://www.crunchbase.com/organization/gualy-payment" target="_blank">
          <img src="img/logo_crunchbase.svg" alt="crunchbase">
        </a>
        <div class="footerContSocial">
          <a href="https://www.instagram.com/gualyapp/" target="_blank">
            <img src="img/Path 148.svg" alt="instagram">
          </a>
          <a href="https://www.facebook.com/Gualy-App-1963067283978287/" target="_blank">
            <img src="img/Path 149.svg" alt="facebook">
          </a>
          <a href="https://twitter.com/gualyapp" target="_blank">
            <img src="img/Trazado 44.svg" alt="twitter">
          </a>
          <a href="https://www.youtube.com/channel/UCUBHzcH9RkWGf-7u6oe3ihg" target="_blank">
            <img src="img/Grupo 120.svg" alt="youtube">
          </a>
          <a href="https://www.linkedin.com/company/gualy-payment/" target="_blank">
            <img src="img/Sustracción 2.svg" alt="linkedin">
          </a>
        </div>
      </div>
    </div>
    <div class="footerBottomLogo">
      <p class="byLogoFooter">by</p>
      <img src="img/White.svg" width="65" height="37" alt="humbee-logo">
    </div>
  </div>
  <div class="footerCopy">
    <p>2019 © Copyright Humbee Partners</p>
  </div>
</footer>