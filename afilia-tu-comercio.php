<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/business.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="mainBusiness">
    <p class="titleSection">Afilia tu negocio</p>
    <p class="subjectText">Comienza a recibir pagos en segundos con gualy. Pre-afilia tu comercio:</p>
    <form action="mail/dataBusiness" method="POST" enctype="multipart/form-data" id="businessForm">
      <div class="businessCommerce">
        <p class="subjectText">DATOS DEL COMERCIO</p>
        <div class="businessCommercePic">
          <button class="businessCommercePicbutton" id="commercePicture">
            <img src="img/uploadPicture.svg" alt="Picture">
            <input type="file" accept="image/*" id="commercePictureInput" name="commercePicture">
          </button>
          <input class="nameCommerce" type="text" placeholder="Nombre de tu Negocio" id="nameCommerce" name="nameCommerce">
          <input class="rifCommerce" type="text" placeholder="R.I.F." id="rif" name="rif">
        </div>
        <div class="businessCommerceCont">
          <div class="businessCommerceContData">
            <input type="text" placeholder="Descripción" id="description" name="description">
            <select id="category" name="category">
              <option value="">Categoría</option>
              <option value="Alimentación">Alimentación</option>
              <option value="Bienes y servicios financieros y no financieros">Bienes y servicios financieros y no financieros</option>
              <option value="Bienes y servicios funtuarios">Bienes y servicios funtuarios</option>
              <option value="Educación">Educación</option>
              <option value="Entretenimiento">Entretenimiento</option>
              <option value="Equipamiento para el hogar">Equipamiento para el hogar</option>
              <option value="Organizaciones sin fines de lucro">Organizaciones sin fines de lucro</option>
              <option value="Salud">Salud</option>
              <option value="Servicios públicos">Servicios públicos</option>
              <option value="Transporte">Transporte </option>
              <option value="Vestido y Calzado">Vestido y Calzado</option>
            </select>
            <input type="text" placeholder="Cuenta bancaria" id="account" name="account">
            <div class="commerceAddres">
              <div class="commerceAddresCheck">
                <label for="checkCommerce1">Comercio Físico</label>
                <input type="radio" id="checkCommerce1" name="checkCommerce" value="1" onclick="enableInputAddress()" checked><label for="checkCommerce1"></label>
              </div>
              <input class="commerceInputSize" type="text" placeholder="Dirección fiscal" id="fiscarlAddress" name="fiscarlAddress">
            </div>
            <div class="commerceAddres">
              <div class="commerceAddresCheck">
                <label for="checkCommerce">Comercio Digital</label>
                <input type="radio" id="checkCommerce" name="checkCommerce" value="2" onclick="disableInputAddress()"><label for="checkCommerce"></label>
              </div>
            </div>
          </div>
          <div class="iframe" id="iframeMaps">
            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15683.686942628296!2d-71.63282664778087!3d10.663182897596688!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2sve!4v1556652947730!5m2!1ses!2sve" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>

      <div class="businessContact">
        <p class="subjectText">DATOS DE CONTACTO</p>
        <div class="businessContactCont">
          <div class="contactInputName">
            <input type="text" placeholder="Nombre" id="name" name="name">
            <input type="text" placeholder="Apellido" id="lastName" name="lastName">
          </div>
          <div class="contactInputCiEmail">
            <div class="contactInputCi">
              <select name="ciType">
                <option value="V-">C.I. V-</option>
                <option value="E-">C.I. E-</option>
              </select>
              <input type="text" placeholder="Cédula" id="ci" name="ci">
            </div>
            <input class="contactInputEmail" type="text" placeholder="Email" id="email" name="email">
          </div>
          <div class="contactInputPhone">
            <input type="text" placeholder="Teléfono" id="phone" name="phone">
          </div>
        </div>
      </div>

      <div class="businessSecurity">
        <p class="subjectText">SEGURIDAD</p>
        <div class="securityPassCont">
          <div class="showPassword">
            <input id="pass" name="pass" type="password" action="show" placeholder="Contraseña">
            <button id="showHidePass" onclick="showPassword()">
              <img id="showHidePassImg" src="img/visibilityOn.svg" alt="show-Pass">
            </button>
          </div>
          <input type="text" placeholder="Pregunta de seguridad" id="question" name="question">
        </div>
        <div class="securityAnswer">
          <input type="text" placeholder="Respuesta" id="answer" name="answer">
        </div>
      </div>

      <div class="businessSubmit">
        <button type="submit" id="submit" name="submit">Enviar</button>
      </div>
    </form>
  </div>

  <div class="gualyPay">
    <p class="titleSection">¿Qué pasa después de recibir un pago por gualy?</p>
    <div class="gualyPayCont">
      <div>
        <img src="img/Grupo 142.svg" alt="g-pay">
        <p>El pago se verá reflejado en tu<br> cuenta gualy en segundos.</p>
      </div>
      <div>
        <img src="img/Grupo 144.svg" alt="g-pay">
        <p>Puedes usar tu saldo gualy para<br> pagar a tus proveedores y<br> empleados.</p>
      </div>
      <div>
        <img src="img/Grupo 143.svg" alt="g-pay">
        <p>También lo puedes retirar a las<br> cuentas bancarias que tengas<br> asociadas.</p>
      </div>
    </div>
  </div>

  <div class="commerceMain">
    <div class="commerce">
      <p class="titleSection">Comercios afiliados</p>
      <div class="commerceCont">
        <img src="img/estacionamiento lago mall.jpg" alt="commerce-logo">
        <img src="img/sambil.jpg" alt="commerce-logo">
        <img src="img/Galerias.jpg" alt="commerce-logo">
        <img src="img/yogourt-boom-gualy.jpg" alt="commerce-logo">
        <img src="img/logo_allgrill.jpg" alt="commerce-logo">
        <img src="img/Franco (1).jpg" alt="commerce-logo">
        <img src="img/profiles_f.jpg" alt="commerce-logo">
        <img src="img/asodepa.jpg" alt="commerce-logo">
        <img src="img/Mi ternerita Norte.jpg" alt="commerce-logo">
        <img src="img/Mi ternerita Loft.jpg" alt="commerce-logo">
        <img src="img/Viveres La Nueva Imagen-01-01.jpg" alt="commerce-logo">
        <img src="img/super-duper-gualy.jpg" alt="commerce-logo">
        <img src="img/fesca.jpg" alt="commerce-logo">
        <img src="img/rostrosradiantes.jpg" alt="commerce-logo">
        <img src="img/tutti-eventi-gualy.jpg" alt="commerce-logo">
        <img src="img/DELMAIZ.jpg" alt="commerce-logo">
        <img src="img/panaderia mamajulia-01.jpg" alt="commerce-logo">
        <img src="img/Vale parking VIP.jpg" alt="commerce-logo">
        <img src="img/jabibi.jpg" alt="commerce-logo">
        <img src="img/azuba.jpg" alt="commerce-logo">
        <img src="img/Formacion panaderia.jpg" alt="commerce-logo">
        <img src="img/Centor de ortodoncia.jpg" alt="commerce-logo">
        <img src="img/lapaleteria-gualy.jpg" alt="commerce-logo">
        <img src="img/comercio__0002_descarga.jpg" alt="commerce-logo">
        <img src="img/arquidiocesis.jpg" alt="commerce-logo">
        <img src="img/padre claret.jpg" alt="commerce-logo">
        <img src="img/lago-box-gualy.jpg" alt="commerce-logo">
        <img src="img/Lobby Café Hotel Tibisay.jpg" alt="commerce-logo">
        <img src="img/mi vaquita.jpg" alt="commerce-logo">
        <img src="img/Puerta 5 xpress.jpg" alt="commerce-logo">
        <img src="img/Automercado_la_economia.jpg" alt="commerce-logo">
        <img src="img/Panaderia el nuevo tiunfo-01.jpg" alt="commerce-logo">
        <img src="img/Z Car's-01.jpg" alt="commerce-logo">
        <img src="img/metalarte.jpg" alt="commerce-logo">
      </div>
    </div>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/pass.js"></script>
  <script src="js/navBarShow.js"></script>
  <script src="js/validateCommerce.js"></script>
</body>

</html>