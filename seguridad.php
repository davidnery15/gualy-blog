<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/contact.css">
  <link rel="stylesheet" href="css/footerLink.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="securityFooter">
    <div class="securityQuestionsCont">
      <p class="titleSection questTextSpace">SEGURIDAD</p>
      <p class="subjectText securityTextSpc">Procedimientos para garantizar la protección de datos personales de clientes</p>
      <div class="howContactCont">

        <div class="howContactQuest">
          <button id="questionButtonShow" onclick="questionButtonShow()">
            <img id="questionButtonImg" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo protegemos tu información financiera?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide" style="display: none">
            <span class="questionSpanAns">La información financiera, sensible al uso sólo puede ser desencriptada por el usuario final desde un equipo seguro, previamente registrado. Para realizar pagos directos con tarjeta de crédito se hace mandatorio el código de seguridad al reverso de la tarjeta. Dicho número no es almacenado en nuestros servidores y requerido en cada transacción bajo la forma antes descrita.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow2" onclick="questionButtonShow2()">
            <img id="questionButtonImg2" width="15" height="15" src="img/Unión 5.svg">
            <p>Cifrado y almacenamiento</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide2" style="display: none">
            <span class="questionSpanAns questTextSpace">La información financiera es encriptada, almacenada y protegida en servidores seguros. En Web, [https] la marca verde de certificado indica que la encriptación está activa.</span>
            <span class="questionSpanAns questTextSpace">Nuestros sistemas de seguridad están basados en Advanced Encryption Standard (AES), también conocido como Rijndael, un esquema de cifrado por bloques capaz de proteger información sensible.</span>
            <span class="questionSpanAns questTextSpace">Recopilamos información sobre usted cuando se registra o utiliza nuestro Servicio. En el momento de registrarse, podemos solicitarle información detallada, incluyendo su nombre, dirección de correo electrónico, dirección postal, número de teléfono, documento oficial de identidad u otra información. También podemos solicitar y recibir información sobre usted de otras fuentes, o utilizar dichas fuentes para confirmar la información que usted nos proporciona.</span>
            <span class="questionSpanAns questTextSpace">Cuando visita nuestro sitio y/o utiliza nuestro Servicio, recogemos de manera automática la información que su ordenador, teléfono móvil u otro dispositivo nos envía. Esta información puede incluir su dirección IP, información de dispositivo (lo que incluye, entre otros, el identificador, nombre y tipo, sistema operativo, información de la red de telefonía móvil), información de registro web estándar, tal como el tipo de navegador y las páginas a las que accede en nuestro sitio.</span>
            <span class="questionSpanAns questTextSpace">Cuando utiliza un dispositivo de localización con nuestro Servicio, podemos recoger datos de ubicación geográfica o utilizar diversos medios para determinar su ubicación, tales como datos de los sensores de su dispositivo que pueden, por ejemplo, proporcionar información sobre torres celulares o puntos de acceso wi-fi cercanos.</span>
            <span class="questionSpanAns questTextSpace">Podemos registrar información usando “cookies”. Las cookies son pequeños ficheros de datos almacenados en su disco duro por un sitio web, que nos ayudan a mejorar nuestro sitio y Servicio y el uso que usted haga de ellos, permitiéndonos reconocer su navegador y capturar y recordar cierta información. Visite nuestra Política de Cookies para información adicional.</span>
            <span class="questionSpanAns questTextSpace">Utilizamos el término “información personal” para describir la información que puede estar asociada con una persona en concreto y que puede utilizarse para identificar a esa persona. No consideramos que información personal incluya aquellos datos que hayan sido anonimizados y que, por tanto, no identifican a un usuario concreto. Tomamos medidas cuidadosas, que revisamos periódicamente, como se describe aquí, para proteger su información personal.</span>
            <span class="questionSpanAns questTextSpace">No vendemos, comerciamos ni transferimos su información personal a terceros. Esto no incluye a terceros que nos asisten en el funcionamiento de nuestro Servicio, en la realización de nuestro negocio o en la asistencia a nuestros miembros, siempre que estos terceros se comprometan a mantener dicha información confidencial y segura, en las mismas condiciones y niveles de protección que le ofrecemos como miembro.</span>
            <span class="questionSpanAns questTextSpace">Almacenamos y procesamos su información personal y relativa a transacciones en nuestros servidores de Estados Unidos y en todos aquellos lugares en los que se encuentran nuestras filiales, socios o proveedores, y protegemos dicha información mediante salvaguardas físicas, electrónicas y procedimentales, que incorporan tecnologías de seguridad probadas, en cumplimiento de la legislación aplicable. Podemos utilizar medidas de seguridad informática tales como firewalls y encriptación de datos, hacer cumplir controles de acceso físico, y autorizar el acceso a su información personal únicamente a aquellas personas que necesitan dicho acceso para llevar a cabo su trabajo. Aquellas personas que tienen acceso a su información personal pasan un examen detallado, son periódicamente reevaluados y están obligados a mantener la confidencialidad de toda la información personal de los miembros. Los riesgos de seguridad existen, y ninguna organización puede prometer lo contrario. Los agentes con malas intenciones pueden derrotar incluso las medidas de seguridad más cuidadosamente aplicadas.</span>
            <span class="questionSpanAns questTextSpace">Almacenamos información personal de forma segura y la conservaremos durante el tiempo que sea necesario para mantener el Servicio, cumplir con nuestras obligaciones legales y/o resolver disputas.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow3" onclick="questionButtonShow3()">
            <img id="questionButtonImg3" width="15" height="15" src="img/Unión 5.svg">
            <p>Protección de tu cuenta</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide3" style="display: none">
            <span class="questionSpanAns questTextSpace">Si tu móvil se perdió, o fue robado o crees que tu cuenta está siendo usada de manera fraudulenta. Llama a nuestro call center o entra a tu cuenta desde la web y desactiva cualquier sesión y dispositivo asociados.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow4" onclick="questionButtonShow4()">
            <img id="questionButtonImg4" width="15" height="15" src="img/Unión 5.svg">
            <p>Protege tu cuenta y transacciones</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide4" style="display: none">
            <span class="questionSpanAns questTextSpace">Gualy no ofrece protección al comprador y vendedor. Cualquier inconveniente entre las partes involucradas no es responsabilidad directa e indirecta de Gualy o su proveedor de servicios. El uso de Gualy a nivel comercial requiere autorización y verificación del comercio.</span>
            <span class="questionSpanAns questTextSpace">Adicionalmente Gualy recomienda aplicar costumbres de seguridad como el bloqueo del equipo, a través de contraseña, gesto o reconocimiento biométrico. Esto agrega una capa más a la seguridad de tus finanzas.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow5" onclick="questionButtonShow5()">
            <img id="questionButtonImg5" width="15" height="15" src="img/Unión 5.svg">
            <p>Soporte y Seguridad</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide5" style="display: none">
            <span class="questionSpanAns questTextSpace">Cualquier duda o preocupación que tengas acerca de la seguridad en Gualy, por favor, contácta a nuestro Help Center a través de cualquiera de los siguientes canales en los horarios correspondientes:</span>
            <div class="securityQuestionLastCont">
              <h4 class="securityTitle">DE 8:00 A.M. A 11:59 P.M.</h4>
              <div class="securityWrapper">
                <div class="suppWrappMediaContent">
                  <img src="img/ic_phone_24px.svg" class="suppMediaPhone" alt="phone-number">
                  <span>+58 261-8085657</span>
                </div>
                <div class="suppWrappMediaContent">
                  <img src="img/ic_email_24px.svg" class="suppMediaMail" alt="Mail">
                  <span>soporte@gualy.com</span>
                </div>
                <div class="suppWrappMediaContent2">
                  <img src="img/whatsapp_icon.png" width="98" height="98" alt="WhatsApp">
                  <span>WhatsApp</span>
                </div>
                <div class="suppWrappMediaContent2">
                  <img src="img/t_logo.png" alt="Telegram">
                  <span>Telegram</span>
                </div>
                <div class="suppWrappMediaContent2">
                  <img src="img/chat.png" alt="Web-Chat">
                  <span>Chat de la web</span>
                </div>
              </div>
            </div>

            <div class="securityQuestionLastCont2">
              <h4 class="securityTitle">SOLO DE 8:00 A.M. A 11:59 P.M. PODRÁS:</h4>
              <div class="securityWrapper">
                <div class="suppWrappMediaContent">
                  <img src="img/Anadir-saldo.png">
                  <span>Añadir Saldo Gualy</span>
                </div>
                <div class="suppWrappMediaSaldo">
                  <img src="img/Retirar-saldo.png">
                  <span>Retirar Saldo Gualy a tu cuenta bancaria</span>
                </div>
              </div>
            </div>
            <span class="questionSpanAns questTextSpace">El equipo de desarrollo de Gualy, es un grupo altamente calificado y está comprometido en ofrecerle el mejor servicio y experiencia de usuario. Nos esforzamos para responder a las necesidades de nuestros clientes de manera rápida y eficiente, garantizando la calidad que nuestros procesos requieren.</span>
          </span>
        </div>

      </div>
    </div>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/questionSecurity.js"></script>
</body>

</html>