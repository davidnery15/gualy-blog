<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/contact.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="contact">
    <p class="titleSection">Contacto</p>
    <form action="mail/dataContact" method="POST" id="contactForm" name="contactForm">
      <div class="inputCont">
        <input class="dateInput" type="text" placeholder="Nombre" id="name" name="name">
        <input class="dateInput" type="text" placeholder="Apellido" id="lastName" name="lastName">
      </div>
      <input class="subject" type="text" placeholder="Correo" id="email" name="mail">
      <textarea cols="30" rows="10" placeholder="Mensaje" name="message"></textarea>
      <button class="submit" type="submit" name="submit">Enviar</button>
    </form>
  </div>

  <div class="supp">
    <p class="titleSection">Soporte</p>
    <div class="suppWrapper">
      <div class="suppWrappMedia">
        <img src="img/ic_phone_24px.svg" class="suppMediaPhone" alt="phone-number">
        <p>+58 261-8085657</p>
      </div>
      <div class="suppWrappMedia">
        <img src="img/ic_email_24px.svg" class="suppMediaMail" alt="Mail">
        <p>soporte@gualy.com</p>
      </div>
      <div class="suppWrappMedia">
        <img src="img/whatsapp_icon.png" width="98" height="98" alt="WhatsApp">
        <p>WhatsApp</p>
      </div>
      <div class="suppWrappMedia">
        <img src="img/t_logo.png" alt="Telegram">
        <p>Telegram</p>
      </div>
      <div class="suppWrappMedia">
        <img src="img/chat.png" alt="Web-Chat">
        <p>Chat de la web</p>
      </div>
    </div>
  </div>

  <div class="contactQuestions">
    <div class="contactQuestionsCont">
      <p class="titleSection">Preguntas frecuentes</p>
      <div class="howContactCont">

        <div class="howContactQuest">
          <button id="questionButtonShow" onclick="questionButtonShow()">
            <img id="questionButtonImg" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Quiénes pueden usar gualy?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide" style="display: none">
            <span class="questionSpanAns">Gualy es una Billetera Digital que puede ser usada por personas naturales y jurídicas (comercios).</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow2" onclick="questionButtonShow2()">
            <img id="questionButtonImg2" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Qué debo hacer para tener una cuenta en gualy?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide2" style="display: none">
            <span class="questionSpanAns questTextSpace">Tener una cuenta en Gualy es muy sencillo, sólo debes seguir los siguientes pasos:</span>
            <span class="questionSpanAns questTextSpace"><strong>Si eres persona natural:</strong></span>
            <span class="questionSpanAns questTextSpace">1) Descarga la Gualy en Google Play o App Store.</span>
            <span class="questionSpanAns questTextSpace">2) Pulsa el botón REGISTRO.</span>
            <span class="questionSpanAns questTextSpace">3) Ingresa tus datos, correo electrónico y número de teléfono.</span>
            <span class="questionSpanAns questTextSpace">4) Coloca tu foto de perfil, tomando una foto con tu dispositivo o seleccionando una de tu galería.</span>
            <span class="questionSpanAns questTextSpace">5) Coloca tu contraseña y selecciona una pregunta secreta de seguridad.</span>
            <span class="questionSpanAns questTextSpace">6) Acepta los términos y condiciones, así como también las políticas de privacidad.</span>
            <span class="questionSpanAns questTextSpace">Así, ya cuentas con tu perfil en Gualy, donde puedes tener tu dinero en un solo lugar.</span>
            <span class="questionSpanAns questTextSpace">Además de tu correo electrónico, cuentas con un Código QR único que también será tu identificador para enviar y solicitar pagos.</span>
            <span class="questionSpanAns questTextSpace"><strong>Si eres persona jurídica (comercio):</strong></span>
            <span class="questionSpanAns questTextSpace">1) Realiza el pre-registro a través de <a href="afilia-tu-comercio">http://gualy.com/afilia-tu-comercio/</a></span>
            <span class="questionSpanAns">2) En breve uno de nuestros especialistas de negocios se comunicará contigo para coordinar el registro de tu empresa.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow3" onclick="questionButtonShow3()">
            <img id="questionButtonImg3" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo agrego dinero en gualy?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide3" style="display: none">
            <span class="questionSpanAns questTextSpace">1) Presiona BALANCE desde la pantalla principal.</span>
            <span class="questionSpanAns questTextSpace">2) Luego pulsa AÑADIR.</span>
            <span class="questionSpanAns questTextSpace">3) Ingresa el monto que deseas añadir.</span>
            <span class="questionSpanAns questTextSpace">4) Elige cómo deseas añadir, por ahora solo está disponible la opción de TRANSFERENCIA.</span>
            <span class="questionSpanAns questTextSpace">5) Toma nota de los datos de la cuenta bancaria de Gualy a la que prefieres transferir.</span>
            <span class="questionSpanAns questTextSpace">6) Ingresa a tu banco y realiza una transferencia con los datos de la cuenta bancaria de Gualy que observaste en la App.</span>
            <span class="questionSpanAns questTextSpace">7) Toma nota del número de transferencia.</span>
            <span class="questionSpanAns questTextSpace">8) Luego vuelve a la App y pulsa ENTENDIDO, QUIERO REPORTAR EL PAGO.</span>
            <span class="questionSpanAns questTextSpace">9) Coloca el Banco al que hiciste la transferencia, el número de la transferencia y la fecha. Verifica todos los datos y si están correctos haz clic en CONFIRMAR Y AÑADIR, si deseas hacer alguna corrección presiona VOLVER.</span>
            <span class="questionSpanAns questTextSpace">10) En breve un agente de atención Gualy recibirá tu reporte de pago, verificará la transferencia en la cuenta bancaria de Gualy y si todo está correcto aprobará tu ingreso de saldo, de lo contrario se comunicará contigo para resolver el problema.</span>
            <span class="questionSpanAns questTextSpace"><strong>- Para registrar tarjetas de crédito:</strong></span>
            <span class="questionSpanAns questTextSpace">Ve a tu perfil y en la sección de Métodos de Pago, ingresa los datos de tu tarjeta de crédito y pulsa AGREGAR.</span>
            <span class="questionSpanAns questTextSpace">Las tarjetas de crédito registradas pueden ser de diferentes bancos y sus datos se guardarán cifrados en nuestra app.</span>
            <span class="questionSpanAns questTextSpace">Con ellas puedes agregar dinero a tu cuenta Gualy.</span>
            <span class="questionSpanAns questTextSpace"><strong>- Para registrar cuentas bancarias::</strong></span>
            <span class="questionSpanAns questTextSpace">Ve a tu perfil y en la sección de Métodos de Pago, ingresa los datos de tu cuenta bancaria y pulsa AGREGAR.</span>
            <span class="questionSpanAns questTextSpace">Las cuentas bancarias registradas pueden ser de diferentes bancos y sus datos se guardarán cifrados en nuestra app.</span>
            <span class="questionSpanAns questTextSpace">Con ellas puedes ingresar dinero a tu cuenta Gualy.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow4" onclick="questionButtonShow4()">
            <img id="questionButtonImg4" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo envío y solicito un pago?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide4" style="display: none">
            <span class="questionSpanAns questTextSpace">En Gualy cuentas con distintas maneras para enviar y solicitar pagos:</span>
            <span class="questionSpanAns questTextSpace"><strong>- Para enviar un pago:</strong></span>
            <span class="questionSpanAns questTextSpace">Ingresa el correo electrónico o escanea el Código QR del usuario que recibirá el dinero y pulsa ENVIAR PAGO.</span>
            <span class="questionSpanAns questTextSpace"><strong>- Para solicitar un pago:</strong></span>
            <span class="questionSpanAns questTextSpace">Ingresa el correo electrónico del usuario que recibirá el dinero o genera un Código QR por el monto solicitado y pulsa SOLICITAR PAGO.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow5" onclick="questionButtonShow5()">
            <img id="questionButtonImg5" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cuánto tiempo toma pagar con gualy?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide5" style="display: none">
            <span class="questionSpanAns">Los pagos a usuarios y comercios se realizan en segundos.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow6" onclick="questionButtonShow6()">
            <img id="questionButtonImg6" width="15" height="15" src="img/Unión 5.svg">
            <p>Si recibo un pago, ¿en cuánto tiempo puedo usar el dinero?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide6" style="display: none">
            <span class="questionSpanAns">Al recibir un pago puedes utilizar de inmediato tu dinero para pagar a otros usuarios o comercios en sus cuentas Gualy.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow7" onclick="questionButtonShow7()">
            <img id="questionButtonImg7" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo puedo ver los detalles de las transacciones?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide7" style="display: none">
            <span class="questionSpanAns questTextSpace">Recibirás notificaciones de cada transacción y en la sección Historial puedes ver el detalle, teniendo el control de todos tus movimientos.</span>
            <span class="questionSpanAns questTextSpace">Además, como comercio afiliado puedes observar el registro de cada acción realizada por los administradores que agregues a tu cuenta Gualy Comercio, asignando los permisos que consideres pertinentes para cada uno.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow8" onclick="questionButtonShow8()">
            <img id="questionButtonImg8" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Qué tarifa cobra gualy por recibir dinero?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide8" style="display: none">
            <span class="questionSpanAns questTextSpace">La tarifa por los servicios de recaudación de Gualy será un % de cada pago que hayas recibido a través de su plataforma.</span>
            <span class="questionSpanAns questTextSpace">Las tarifas para personas naturales estarán publicadas en www.gualy.com en la sección de comisiones.</span>
            <span class="questionSpanAns questTextSpace">Las tarifas para personas jurídicas (comercios) debe ser consultada al correo info@gualy.com ya que dependerá del tipo de actividad económica y volumen de ventas.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow9" onclick="questionButtonShow9()">
            <img id="questionButtonImg9" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo se puede retirar el dinero?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide9" style="display: none">
            <span class="questionSpanAns questTextSpace">Puedes solicitar en cualquier momento el traslado de saldo que tienes disponible en tu cuenta Gualy a cualquiera de las cuentas bancarias propias que tengas registradas en tu perfil.</span>
            <span class="questionSpanAns questTextSpace">Toma en cuenta que las transferencias pueden tardar hasta 72 horas en hacerse efectivas en tu cuenta bancaria, este tiempo dependerá de la plataforma bancaria.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow10" onclick="questionButtonShow10()">
            <img id="questionButtonImg10" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Aceptan moneda extranjera?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide10" style="display: none">
            <span class="questionSpanAns">No, Gualy trabaja con transacciones en la moneda local venezolana (VEF / VES).</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow11" onclick="questionButtonShow11()">
            <img id="questionButtonImg11" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo puedo ver o editar la información de mi perfil?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide11" style="display: none">
            <span class="questionSpanAns">En la sección de Ajustes puedes modificar o actualizar tu información de perfil en cualquier momento.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow12" onclick="questionButtonShow12()">
            <img id="questionButtonImg12" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo puedo restablecer mi contraseña?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide12" style="display: none">
            <span class="questionSpanAns">En el inicio debes pulsar OLVIDÉ MI CONTRASEÑA y recibirás un correo para crear una nueva contraseña.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow13" onclick="questionButtonShow13()">
            <img id="questionButtonImg13" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo puedo solicitar ayuda o realizar un reclamo?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide13" style="display: none">
            <span class="questionSpanAns">Si necesitas AYUDA puedes solicitar soporte técnico, enviar comentario o reportar un problema en la sección Ayuda de la aplicación. También puedes comunicarte con nuestro Help Center a través del correo soporte@gualy.com, el número telefónico 0261-8085657 y el chat en www.gualy.com.</span>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow14" onclick="questionButtonShow14()">
            <img id="questionButtonImg14" width="15" height="15" src="img/Unión 5.svg">
            <p>¿Cómo verifico mi usuario?</p>
          </button>
          <span class="howQuestCont2" id="questionShowHide14" style="display: none">
            <span class="questionSpanAns questTextSpace">Envía a soporte@gualy.com los siguientes documentos en JPG, PNG o PDF:</span>
            <span class="questionSpanAns questTextSpace">1) Documento de identidad: envía la foto de tu cédula de identidad o pasaporte vigente.</span>
            <span class="questionSpanAns questTextSpace">2) Dirección:</span>
            <span class="questionSpanAns questTextSpace">– Envía uno de estos documentos donde se observe la dirección. Puede ser RIF, factura de servicio público o privado o estado de cuenta bancaria.</span>
            <span class="questionSpanAns questTextSpace">– Debe haber sido emitido con fecha menor a 6 meses.</span>
            <span class="questionSpanAns questTextSpace">– Debe estar a tu nombre.</span>
            <span class="questionSpanAns questTextSpace">– La dirección colocada en tu perfil debe estar igual al documento.</span>
            <span class="questionSpanAns questTextSpace">3) Para verificar tu cuenta bancaria, agrega un soporte bancario, puede ser un cheque inutilizado, referencia bancaria o estado de cuenta donde puedan leerse los 20 dígitos de tu cuenta bancaria.</span>
          </span>
        </div>

      </div>
    </div>
  </div>

  <div class="prensa">
    <p class="titleSection">Prensa</p>
    <p class="subjectText">Descarga la información que necesitas sobre gualy, acá.</p>
    <div class="prensaContImg">
      <a href="files/Prensa.rar">
        <img src=" img/Grupo 148.png" alt=".JPG">
      </a>
      <a href="files/Prensa.docx">
        <img src="img/Grupo 140.png" alt=".DOC">
      </a>
      <a href="files/Prensa.pdf" download="Prensa">
        <img src="img/Grupo 147.png" alt=".PDF">
      </a>
    </div>
  </div>

  <div class="rate">
    <p class="titleSection">Tarifas</p>
    <p class="subjectText">Envía y Recibe pagos GRATIS con gualy, sin comisiones ni tarifas.</p>
  </div>

  <div class="investment">
    <p class="titleSection">Inversión</p>
    <p class="subjectText">Déjanos tus datos y te contaremos más sobre nuestros planes de inversión.</p>
    <form action="mail/dataContact1" method="POST" id="contactForm1" name="contactForm1">
      <div class="inputCont">
        <input class="dateInput" type="text" placeholder="Nombre" id="name1" name="name1">
        <input class="dateInput" type="text" placeholder="Apellido" id="lastName1" name="lastName1">
      </div>
      <input class="subject" type="text" placeholder="Correo" id="email1" name="mail1">
      <textarea cols="30" rows="10" placeholder="Mensaje" name="message1"></textarea>
      <button class="submit" type="submit" name="submit1">Enviar</button>
    </form>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/questionContact.js"></script>
  <script src="js/validateContact.js"></script>
  <script src="js/validateContact1.js"></script>
</body>

</html>