<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gualy - App Móvil y Web Fintech - Billetera Virtual - Solución Financiera</title>
  <link rel="stylesheet" href="css/styles.css">
  <link rel="stylesheet" href="css/howItWorkStyles.css">
  <link rel="icon" type="image/x-icon" href="img/gualy icon app-01.png">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
</head>

<body>

  <?php
  include_once("header.php");
  ?>

  <div class="howItWork">
    <div class="howContactMain">
      <p class="titleSection">¿Cómo funciona?</p>
      <div class="howContactCont">

        <div class="howContactQuest">
          <button id="questionButtonShow" onclick="questionButtonShow()">
            <img id="questionButtonImg" width="15" height="15" src="img/Unión 5.svg">
            <p>Enviar pagos</p>
          </button>
          <span class="howQuestCont" id="questionShowHide" style="display: none">
            <div class="howQuestContAns">
              <h3>ENVÍA PAGOS SIGUIENDO ESTOS PASOS:</h3>
              <ol>
                <li>Presiona ENVIAR PAGO desde la pantalla principal</li>
                <li>Ingresa el monto que deseas enviar</li>
                <li>Coloca la descripción del pago</li>
                <li>Escribe o selecciona desde el directorio el email del usuario, o escanea su código QR</li>
                <li>Presiona ENVIAR PAGO</li>
                <li>Verifica todos los datos y luego presiona DE ACUERDO</li>
              </ol>
              <p>* Recuerda que para enviar pagos debes tener saldo en tu cuenta Gualy</p>
            </div>
            <iframe src="https://marvelapp.com/39a459c?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow2" onclick="questionButtonShow2()">
            <img id="questionButtonImg2" width="15" height="15" src="img/Unión 5.svg">
            <p>Solicitar Pagos</p>
          </button>
          <span class="howQuestCont" id="questionShowHide2" style="display: none">
            <div class="howQuestContAns">
              <h3>SOLICITA PAGOS SIGUIENDO ESTOS PASOS:</h3>
              <ol>
                <li>Presiona SOLICITAR PAGO desde la pantalla principal</li>
                <li>Ingresa el monto que deseas solicitar</li>
                <li>Coloca la descripción de la solicitud</li>
                <li>Escribe o selecciona desde el directorio el email del usuario, o genera un código QR para que lo escanee</li>
                <li>Presiona SOLICITAR PAGO</li>
                <li>Verifica todos los datos de la solicitud, y luego presiona DE ACUERDO</li>
              </ol>
            </div>
            <iframe src="https://marvelapp.com/dfj1ad9?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow3" onclick="questionButtonShow3()">
            <img id="questionButtonImg3" width="15" height="15" src="img/Unión 5.svg">
            <p>Historial</p>
          </button>
          <span class="howQuestCont" id="questionShowHide3" style="display: none">
            <div class="howQuestContAns">
              <h3>PARA OBSERVAR LOS MOVIMIENTOS DE TU CUENTA EN EL HISTORIAL SIGUE ESTOS PASOS:</h3>
              <ol>
                <li>Presiona HISTORIAL desde la pantalla principal</li>
                <li>Luego pulsa sobre la opción que quieres ver:</li>
                <ul class="questListNoOrder">
                  <li>INGRESOS: verás todos los pagos que has recibido y los ingresos de saldo que hayas realizado.</li>
                  <li>EGRESOS: verás todos los pagos que has enviado y los retiros de saldo que hayas realizado.</li>
                  <li>TODAS: verás todos los pagos que hayas recibido y enviado, los ingresos y retiros de saldo que hayas realizado y las solicitudes de pago que hayas enviado y recibido.</li>
                  <li>Adicionalmente, al lado de cada movimiento verás una de estas dos marcas:</li>
                  <li class="questListNoOrderSpace"><img class="questListNoOrderImg" src="img/shape.png">significa aprobada</li>
                  <li><img class="questListNoOrderImg" src="img/shape-1.png">significa rechazada</li>
                </ul>
              </ol>
              <ol>
                <li>Haz clic sobre cada movimiento para ver los detalles.</li>
                <li>Si tienes problemas con algún movimiento pulsa REPORTAR UN PROBLEMA y escribe lo que ocurrió, si lo deseas también puedes adjuntar una imagen y luego presiona CONFIRMAR Y ENVIAR REPORTE, en breve un agente de atención Gualy se comunicará contigo para ayudarte a resolverlo.</li>
              </ol>
            </div>
            <iframe src="https://marvelapp.com/18123c3g?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow4" onclick="questionButtonShow4()">
            <img id="questionButtonImg4" width="15" height="15" src="img/Unión 5.svg">
            <p>Añadir saldo</p>
          </button>
          <span class="howQuestCont" id="questionShowHide4" style="display: none">
            <div class="howQuestContAns">
              <h3>AÑADE SALDO A TU CUENTA SIGUIENDO ESTOS PASOS:</h3>
              <ol>
                <li>Presiona <strong>BALANCE</strong> desde la pantalla principal</li>
                <li>Luego pulsa <strong>AÑADIR</strong></li>
                <li>Ingresa el monto que deseas añadir</li>
                <li>Elige cómo deseas añadir, por ahora solo está disponible la opción de <strong>TRANSFERENCIA</strong></li>
                <li>Toma nota de los datos de la cuenta bancaria de Gualy a la que prefieres transferir</li>
                <li>Ingresa a tu banco y realiza una transferencia con los datos de la cuenta bancaria de Gualy que observaste en la app</li>
                <li>Luego vuelve a la app y pulsa <strong>ENTENDIDO, QUIERO REPORTAR EL PAGO</strong></li>
                <li>Verifica todos los datos y si están correctos haz clic en <strong>CONFIRMAR Y AÑADIR</strong>, si deseas hacer alguna corrección presiona <strong>VOLVER</strong></li>
                <li>Un agente de atención Gualy recibirá tu reporte de pago, verificará la transferencia en la cuenta bancaria de Gualy y si todo está correcto aprobará tu ingreso de saldo, de lo contrario se comunicará contigo para resolver el problema.</li>
              </ol>
            </div>
            <iframe src="https://marvelapp.com/e0gid4j?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow5" onclick="questionButtonShow5()">
            <img id="questionButtonImg5" width="15" height="15" src="img/Unión 5.svg">
            <p>Retirar saldo</p>
          </button>
          <span class="howQuestCont" id="questionShowHide5" style="display: none">
            <div class="howQuestContAns">
              <h3>RETIRA TU SALDO GUALY A TU CUENTA BANCARIA SIGUIENDO ESTOS PASOS:</h3>
              <ol>
                <li>Presiona <strong>BALANCE</strong> desde la pantalla principal</li>
                <li>Luego pulsa <strong>RETIRAR</strong></li>
                <li>Ingresa el monto que deseas retirar</li>
                <li>Haz clic sobre la cuenta bancaria en la deseas recibir el dinero</li>
                <li>Pulsa <strong>CONFIRMAR Y RETIRAR</strong></li>
              </ol>
              <p class="questTextSpace">*Los retiros pueden tardar hasta 72 horas en hacerse efectivos en tu cuenta bancaria, dependerá de los tiempos propios de cada banco.</p>
              <p>*Sólo podrás retirar saldo a las cuentas bancarias que hayas agregado en tu cuenta Gualy.</p>
            </div>
            <iframe src="https://marvelapp.com/e0gid4j?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow6" onclick="questionButtonShow6()">
            <img id="questionButtonImg6" width="15" height="15" src="img/Unión 5.svg">
            <p>Compartir tu código</p>
          </button>
          <span class="howQuestCont" id="questionShowHide6" style="display: none">
            <div class="howQuestContAns">
              <h3>SIGUE ESTOS PASOS PARA COMPARTIR TU CÓDIGO QR:</h3>
              <ol>
                <li>Presiona tu foto desde la pantalla principal para ir tu <strong>PERFIL</strong></li>
                <li>Luego pulsa <strong>MI CÓDIGO</strong></li>
                <li>Haz clic sobre el símbolo de compartir</li>
                <li>Selecciona el medio que deseas usar para enviarlo</li>
              </ol>
            </div>
            <iframe src="https://marvelapp.com/979d564?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow7" onclick="questionButtonShow7()">
            <img id="questionButtonImg7" width="15" height="15" src="img/Unión 5.svg">
            <p>Ver los comercios</p>
          </button>
          <span class="howQuestCont" id="questionShowHide7" style="display: none">
            <div class="howQuestContAns">
              <h3>PARA VER LOS COMERCIOS QUE ACEPTAN GUALY COMO MÉTODO DE PAGO SIGUE ESTOS PASOS:</h3>
              <ol>
                <li>Presiona <img src="img/location_white.png"> desde la pantalla principal</li>
                <li>Luego escribe la distancia en Km a la que quieres ver los comercios</li>
                <li>Para:</li>
                <ul class="questListNoOrder">
                  <li>3.1. Buscar un tipo de comercio o un comercio en específico pulsa la <img src="img/ic_search_wh.png"> y escribe lo que estás buscando</li>
                  <li>3.2. Enviar un pago a comercios: selecciona el comercio, coloca la cantidad de Bs que deseas enviar y presiona ENVIAR PAGO</li>
                </ul>
              </ol>
            </div>
            <iframe src="https://marvelapp.com/4054h1a?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow8" onclick="questionButtonShow8()">
            <img id="questionButtonImg8" width="15" height="15" src="img/Unión 5.svg">
            <p>Directorio</p>
          </button>
          <span class="howQuestCont" id="questionShowHide8" style="display: none">
            <div class="howQuestContAns">
              <h3>AGREGA O ELIMINA CONTACTOS DE TU DIRECTORIO SIGUIENDO ESTOS PASOS:</h3>
              <h4 class="questSubtitle questTextSpace">AGREGAR</h4>
              <ol>
                <li>Presiona <img src="img/page_1.png"> desde la pantalla principal</li>
                <li>Luego pulsa <img src="img/ic_add.png"></li>
                <li>Escribe el email del usuario que deseas agregar a tu directorio</li>
                <li>Presiona AGREGAR</li>
              </ol>
              <p class="questTextSpace">*Ahora cuando ENVÍES o SOLICITES pagos, en lugar de escribir el correo solo necesitas pulsar <img src="img/page_1.png"> y seleccionar el usuario.</p>
              <h4 class="questSubtitle questTextSpace">ELIMINAR</h4>
              <ol>
                <li>Presiona <img src="img/page_1.png"> desde la pantalla principal</li>
                <li>Luego pulsa <img src="img/ic_delete_24_px.png"></li>
                <li>Verifica que has seleccionado el usuario que deseas eliminar de tu directorio y presiona SI</li>
              </ol>
            </div>
            <iframe src="https://marvelapp.com/32740b2?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow9" onclick="questionButtonShow9()">
            <img id="questionButtonImg9" width="15" height="15" src="img/Unión 5.svg">
            <p>Registro</p>
          </button>
          <span class="howQuestCont" id="questionShowHide9" style="display: none">
            <div class="howQuestContAns">
              <h3>REALIZA TU REGISTRO SIGUIENDO ESTOS PASOS:</h3>
              <ol>
                <li>Presiona REGISTRO</li>
                <li>Pulsa sobre la <img src="img/upload_picture_315.png"> para agregar tu foto de perfil, y elige si quieres tomar una foto o seleccionar una de la galería</li>
                <li>Escribe tus datos: nombre, apellido, email, cédula de identidad y número telefónico</li>
                <li>Escribe tu contraseña</li>
                <li>Selecciona una pregunta de seguridad</li>
                <li>Escribe la respuesta a la pregunta de seguridad que seleccionaste</li>
                <li>Presiona LISTO</li>
                <li>Ingresa en tu correo, abre el email de bienvenida a Gualy y pulsa VERIFICAR MI CORREO ELECTRÓNICO</li>
                <li>Te llegará un nuevo correo de Gualy indicando que tu correo ha sido verificado exitosamente.</li>
                <li>Ingresa a Gualy con tu usuario y contraseña</li>
              </ol>
            </div>
            <iframe src="https://marvelapp.com/3a2h035?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

        <div class="howContactQuest">
          <button id="questionButtonShow10" onclick="questionButtonShow10()">
            <img id="questionButtonImg10" width="15" height="15" src="img/Unión 5.svg">
            <p>Agregar y eliminar cuentas bancarias</p>
          </button>
          <span class="howQuestCont" id="questionShowHide10" style="display: none">
            <div class="howQuestContAns">
              <h3>AGREGA CUENTAS BANCARIAS SIGUIENDO ESTOS PASOS:</h3>
              <ol>
                <li>Presiona tu foto desde la pantalla principal para ir tu PERFIL</li>
                <li>Luego en CUENTAS BANCARIAS pulsa el símbolo <img src=""></li>
                <li>Escribe el número de cuenta</li>
                <li>Haz clic en AGREGAR</li>
              </ol>
              <p class="questTextSpace2">*Sólo podrás agregar tus cuentas bancarias</p>
              <h3>ELIMINA TUS CUENTAS BANCARIAS SIGUIENDO ESTOS PASOS:</h3>
              <ol>
                <li>Presiona tu foto desde la pantalla principal para ir tu PERFIL</li>
                <li>Presiona un par de segundos sobre la imagen de la cuenta bancaria que deseas eliminar hasta que le aparezca el aviso CONFIRME QUE DESEA ELIMINAR ESTA CUENTA BANCARIA</li>
                <li>Presiona ELIMINAR</li>
              </ol>
            </div>
            <iframe src="https://marvelapp.com/971827b?emb=1&iosapp=false&frameless=false" width="260" height="470" allowTransparency="true" frameborder="0"></iframe>
          </span>
        </div>

      </div>
    </div>
  </div>

  <?php
  include_once("footer.php");
  ?>

  <script src="js/chat.js"></script>
  <script src="js/questionShow.js"></script>
</body>

</html>